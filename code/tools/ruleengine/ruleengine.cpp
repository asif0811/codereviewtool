
#include <string>
#include <iostream>
#include <assert.h>
#include <algorithm>

#include "oclExpressionParser.hpp"
#include "oclExpressionLexer.hpp"
#include "oclExpression.h"
#include "prrRule.h"

namespace PRR
{
Rule* generateRule(std::string& statement, std::string& errorstring)
    {
    PRR::Rule* rule = NULL;

    try
        {
            ANTLR_UINT8* rulestatement = (ANTLR_UINT8*)statement.c_str();
            OCLParser::oclExpressionLexer::InputStreamType input(rulestatement,ANTLR_ENC_8BIT,(ANTLR_UINT32)strlen(statement.c_str()),(ANTLR_UINT8*)"rulestmnt");
            OCLParser::oclExpressionLexer lxr(&input);
            OCLParser::oclExpressionParser::TokenStreamType tstream(ANTLR_SIZE_HINT, lxr.get_tokSource() );
            OCLParser::oclExpressionParser psr(&tstream);
            
            OCL::Expression* expression = psr.expression();
            if (expression != NULL)
 	        {
                rule = new PRR::Rule(expression);
            }
            else
            {
                errorstring = "Error while parsing the rule...!!!";
            }
        }
    catch(...)
        {
        errorstring = "Error while parsing the rule : exception thrown while creating the rule";
        std::cout << std::endl << errorstring;
        }

    return rule;
    }
}

