//OCL expressions implementation

#include <iostream>

#include "oclExpression.h"
#include "prrRule.h"
#include "prrRuleInterface.h"

namespace OCL
{
//implementation of base OCL expression
Expression::Expression(IfExpression* ifthenexpression)
    {
    m_ifthenexpression = ifthenexpression;
    m_simpleexpression = NULL;
    m_errormessage = std::wstring();
    m_warningmessage = std::wstring();
    }

Expression::Expression(LogicalExpression* simpleexpression)
    {
    m_simpleexpression = simpleexpression;
    m_ifthenexpression = NULL;
    m_errormessage = std::wstring();
    m_warningmessage = std::wstring();
    }

Expression::~Expression()
    {
    if (m_simpleexpression)
        delete m_simpleexpression;
    if (m_ifthenexpression)
        delete m_ifthenexpression;
    }

Any Expression::evaluate()
    {
    Any result;
    if (m_ifthenexpression)
        result = m_ifthenexpression->evaluate();
    else if (m_simpleexpression)
        result = m_simpleexpression->evaluate();

    return result;
    }
    
void Expression::setErrorMessage(std::wstring message)
    {
    m_errormessage = message;
    }

std::wstring Expression::getErrorMessage()
    {
    return m_errormessage;
    }

void Expression::setWarningMessage(std::wstring message)
    {
    m_warningmessage = message;
    }

std::wstring Expression::getWarningMessage()
    {
    return m_warningmessage;
    }

//implementation of If-then expression
IfExpression::IfExpression(LogicalExpression* ifcondition, LogicalExpression* thenexpression)
    {
    m_ifcondition = ifcondition;
    m_thenexpression = thenexpression;
    }

IfExpression::~IfExpression()
    {
    if (m_ifcondition)
        delete m_ifcondition;
    if (m_thenexpression)
        delete m_thenexpression;
    }

Any IfExpression::evaluate()
    {
    Any result;
    if (m_ifcondition)
        {
        result = m_ifcondition->evaluate();
        if (result.boolresult() && !result.IsInvalid())
            result = m_thenexpression->evaluate(true);
        else
            return Invalid();
        }

    return result;
    }

//implementation of logical OCL expression
void LogicalExpression::add(RelationalExpressionStruct* relationaloperation)
    {
    m_relationalexpressions.push_back(relationaloperation);
    }

LogicalExpression::~LogicalExpression()
    {
    std::vector<RelationalExpressionStruct*>::iterator it = m_relationalexpressions.begin();
    while(it != m_relationalexpressions.end())
    {
        RelationalExpressionStruct *relationalExpression = *it;
        ++it;
        if (relationalExpression != NULL)
            delete relationalExpression;
    }
    }

LogicalExpression::RelationalExpressionStruct::RelationalExpressionStruct(RelationalExpression* prelationalexpression, LogicalOperator logicaloper)
    {
    relationalexpression = prelationalexpression;
    logicaloperator = logicaloper;
    }
    
LogicalExpression::RelationalExpressionStruct::~RelationalExpressionStruct()
    {
        if (relationalexpression)
            delete relationalexpression;
    }

Any LogicalExpression::evaluate(bool then_expression)
    {
    Any result;
    std::vector<RelationalExpressionStruct*>::iterator it = m_relationalexpressions.begin();
    if (it == m_relationalexpressions.end())
        {
        result.m_value.boolean = true;
        return result;
        }

    result = (*it)->relationalexpression->evaluate(then_expression);
    it++;
    while(it != m_relationalexpressions.end() && !result.IsInvalid())
        {
        if (!result.boolresult() && (*it)->logicaloperator == LOG_AND)
            break;

        Any result2nd = (*it)->relationalexpression->evaluate(then_expression);
        if ((*it)->logicaloperator == LOG_AND)
            result = result & result2nd;
        else if ((*it)->logicaloperator == LOG_OR)
            result = result | result2nd;
        it++;
        }
    return result;
    }


//implementation of relational OCL expression
RelationalExpression::RelationalExpression(AdditiveExpression* leftadditiveexpression, RelationalOperator relationaloperator, AdditiveExpression* rightadditiveexpression)
    {
    m_leftadditiveexpression = leftadditiveexpression;
    m_relationaloperator = relationaloperator;
    m_rightadditiveexpression = rightadditiveexpression;
    }

RelationalExpression::~RelationalExpression()
    {
    if (m_leftadditiveexpression)
        delete m_leftadditiveexpression;
    if (m_rightadditiveexpression)
        delete m_rightadditiveexpression;
    }

Any RelationalExpression::evaluate(bool then_expression)
    {
    Any result;
    Any left = m_leftadditiveexpression->evaluate();
    Any right = m_rightadditiveexpression ? m_rightadditiveexpression->evaluate() : Any();
        
    if (then_expression)
    {
        if (left.strresult().empty() && right.strresult().compare(L"true") == 0)
            left = Any(SString(L"false"));
    }

    switch(m_relationaloperator)
        {
        case RELOP_NONE:
            result = left;
            break;
        case RELOP_EQUAL:
            result = left == right;
            break;
        case RELOP_NOTEQUAL:
            result = left != right;
            break;  
        case RELOP_LESSTHAN:
            result = left < right;
            break;
        case RELOP_LESTTHANEQUAL:
            result = left <= right;
            break;
        case RELOP_GREATERTHAN:
            result = left > right;
            break;
        case RELOP_GREATERTHANEQUAL:
            result = left >= right;
            break;
        case RELOP_STARTSWITH:
            result = left.startsWith(right);
            break;
        }

    return result;
    }


//implementation of additive OCL expression
AdditiveExpression::~AdditiveExpression()
    {
    std::vector<MultiplicativeExpressionStruct*>::iterator it = m_multiplicativeexpressions.begin();
    while(it != m_multiplicativeexpressions.end())
    {
        MultiplicativeExpressionStruct *multExpression = *it;
        ++it;
        if (multExpression != NULL)
            delete multExpression;
    }
    }

void AdditiveExpression::add(MultiplicativeExpressionStruct* multiplicativeoperation)
    {
    m_multiplicativeexpressions.push_back(multiplicativeoperation);
    }

AdditiveExpression::MultiplicativeExpressionStruct::MultiplicativeExpressionStruct(MultiplicativeExpression* pmultexpression, AdditiveOperator addoper)
    {
    multiplicativeexpression = pmultexpression;
    additiveoperator = addoper;
    }

AdditiveExpression::MultiplicativeExpressionStruct::~MultiplicativeExpressionStruct()
    {
        if (multiplicativeexpression)
            delete multiplicativeexpression;
    }

Any AdditiveExpression::evaluate()
    {
    Any result;
    std::vector<MultiplicativeExpressionStruct*>::iterator it = m_multiplicativeexpressions.begin();
    if (it != m_multiplicativeexpressions.end())
        result = (*it)->multiplicativeexpression->evaluate();

    Any right;
    for (it = m_multiplicativeexpressions.begin() + 1; it != m_multiplicativeexpressions.end(); ++it)
        {
        if (result.IsInvalid())
            break;

        right = (*it)->multiplicativeexpression->evaluate();

        if ((*it)->additiveoperator == ADDITIVE_ADD)
            result = result + right;
        else if ((*it)->additiveoperator == ADDITIVE_SUBTRACT)
            result = result - right;
        }

    return result;
    }


//implementation of multiplicative OCL expression
MultiplicativeExpression::~MultiplicativeExpression()
    {
    std::vector<UnaryExpressionStruct*>::iterator it = m_unaryexpressions.begin();
    while(it != m_unaryexpressions.end())
    {
        UnaryExpressionStruct *unaryExpression = *it;
        ++it;
        if (unaryExpression != NULL)
            delete unaryExpression;
    }
    }

void MultiplicativeExpression::add(UnaryExpressionStruct* unaryoperation)
    {
    m_unaryexpressions.push_back(unaryoperation);
    }

MultiplicativeExpression::UnaryExpressionStruct::UnaryExpressionStruct(UnaryExpression* punaryexpression, MultiplicativeOperator multoper)
    {
    unaryexpression = punaryexpression;
    multiplicativeoperator = multoper;
    }

MultiplicativeExpression::UnaryExpressionStruct::~UnaryExpressionStruct()
    {
        if (unaryexpression)
            delete unaryexpression;
    }

Any MultiplicativeExpression::evaluate()
    {
    Any result;
    std::vector<UnaryExpressionStruct*>::iterator it = m_unaryexpressions.begin();
    if (it != m_unaryexpressions.end())
        result = (*it)->unaryexpression->evaluate();

    Any right;
    for (it = m_unaryexpressions.begin() + 1; it != m_unaryexpressions.end(); ++it)
        {
        if (result.IsInvalid())
            break;

        right = (*it)->unaryexpression->evaluate();

        if ((*it)->multiplicativeoperator == MULTIPLICATIVE_MULTIPLY)
            result = result * right;
        else if ((*it)->multiplicativeoperator == MULTIPLICATIVE_DIVIDE)
            result = result / right;
        }

    return result;
    }


//implementation of unary OCL expression
UnaryExpression::~UnaryExpression()
    {
    if (m_postfixexpression)
        delete m_postfixexpression;
    }

void UnaryExpression::add(PostfixExpression* postfixexpression)
    {
    m_postfixexpression = postfixexpression;
    }

Any UnaryExpression::evaluate()
    {
    Any result;
    if (m_postfixexpression)
        result = m_postfixexpression->evaluate();

    int stacksize = m_unaryoperators.size();
    while (stacksize-- > 0)
        {
        UnaryOperator unaryop = m_unaryoperators.top();
        if (unaryop == UNARY_NOT)
            result.m_value.boolean = result.m_value.boolean.NOT(result.m_value.boolean);
        }

    return result;
    }


//implementation of postfix OCL expression
PostfixExpression::PostfixExpression(PrimaryExpression* primaryexpression)
    {
    m_primaryexpression = primaryexpression;
    }

PostfixExpression::~PostfixExpression()
    {
    if (m_primaryexpression)
        delete m_primaryexpression;
    }

Any PostfixExpression::evaluate()
    {
    Any result;
    if (m_primaryexpression)
        result = m_primaryexpression->evaluate();

    return result;
    }


//implementation of primary expression
PrimaryExpression::PrimaryExpression()
    {
    m_primaryexpressionstruct.oclliteral = NULL;
    m_primaryexpressionstruct.featurecall = NULL;
    }

PrimaryExpression::~PrimaryExpression()
    {
    if (m_primaryexpressionstruct.oclliteral)
        delete m_primaryexpressionstruct.oclliteral;
    if (m_primaryexpressionstruct.featurecall)
        delete m_primaryexpressionstruct.featurecall;
    }

void PrimaryExpression::add(Literal* pliteral)
    {
    m_primaryexpressionstruct.oclliteral = pliteral;
    }

void PrimaryExpression::add(FeatureCall* pfeaturecall)
    {
    m_primaryexpressionstruct.featurecall = pfeaturecall;
    }

Any PrimaryExpression::evaluate()
    {
    Any result;
    if (m_primaryexpressionstruct.oclliteral)
        result = m_primaryexpressionstruct.oclliteral->evaluate();
    else if(m_primaryexpressionstruct.featurecall)
        result = m_primaryexpressionstruct.featurecall->evaluate();

    return result;
    }


//implementation of Literal
Literal::Literal(std::wstring value, Type type)
    {
    m_strval = m_hashname = std::wstring();
    m_numval = 0;

    m_type = type;
    switch(m_type)
        {
        case OCLSTRING:
            m_strval = value;
            m_strval = m_strval.substr(1, m_strval.length() - 2);
            break;
        case OCLHASHNAME:
            m_hashname = value;
            break;
        case OCLNUMBER:
            wchar_t *tail;
            std::wstring numstr = value;
            m_numval = wcstol(numstr.c_str(), &tail, 10);
            break;
        }
    }

Any Literal::evaluate()
    {
    Any result;
    switch(m_type)
        {
        case OCLSTRING:
            result.m_value.string = m_strval;
            break;
        case OCLHASHNAME:
            result.m_value.string = m_hashname;
            break;
        case OCLNUMBER:
            result.m_value.real = (int)m_numval;
            //result.m_value.integer = m_numval;
            break;
        }

    return result;
    }


//implementation of FeatureCall
FeatureCall::FeatureCall(std::wstring featurename, std::wstring methodname)
    {
    m_featurename = featurename;
    m_methodname = methodname;
    }

Any FeatureCall::evaluate()
    {
    Any result;
    PRR::RuleContext* rulecontext = PRR::RuleContext::getInstance();
    PRR::RuleVariable* rulevariable = rulecontext->getrulevariable();
    
    if(rulevariable)
        {
        PRR::RuleInterface* object = rulevariable->get(m_featurename);
        if (object)
            object->evaluate(m_methodname,result);
        }

    return result;
    }

}



