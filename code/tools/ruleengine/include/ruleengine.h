#ifndef __PRRRULEENGINE
#define __PRRRULEENGINE

#include <string>

namespace PRR
{
class Rule;

Rule* generateRule(std::string& statement, std::string& errorstring);

}

#endif
