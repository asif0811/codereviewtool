//enums for OCL expression operators

#ifndef __OCLEXPRESSIONOPERATORS
#define __OCLEXPRESSIONOPERATORS

namespace OCL
{
//enum representing logical OCL expression operators
enum LogicalOperator { LOG_NONE, LOG_AND, LOG_OR };

//enum representing relational OCL expression operators
enum RelationalOperator { RELOP_NONE, RELOP_EQUAL, RELOP_NOTEQUAL, RELOP_LESSTHAN, RELOP_LESTTHANEQUAL, RELOP_GREATERTHAN, RELOP_GREATERTHANEQUAL, RELOP_STARTSWITH };

//enum representing additive OCL expression operators
enum AdditiveOperator { ADDITIVE_NONE, ADDITIVE_ADD, ADDITIVE_SUBTRACT };

//enum representing multiplicative OCL expression operators
enum MultiplicativeOperator { MULTIPLICATIVE_NONE, MULTIPLICATIVE_MULTIPLY, MULTIPLICATIVE_DIVIDE };

//enum representing unary OCL expression operators
enum UnaryOperator { UNARY_MINUS, UNARY_NOT };

}

#endif

