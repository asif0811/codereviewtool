//Classes representing the PRR rule classes

#ifndef __PRRRULE
#define __PRRRULE

#include <string>
#include <vector>
#include <map>
#include <sstream>

#define RULEENGINEEXPORT
#define STDCALLCONV

#ifdef _WIN32
#define RULEENGINEEXPORT  __declspec(dllexport)
#define STDCALLCONV __stdcall
#endif

namespace OCL
{
class Expression;
}

namespace PRR
{
//class representing a PRR rule
class Rule
    {
    OCL::Expression* m_expression;

    public:
        enum EVAL_RESULT { PASS, FAIL, NOEXECUTE };
        Rule(OCL::Expression* expression);
        virtual ~Rule();

        Rule::EVAL_RESULT evaluate();
        
        std::wstring getErrorMessage();
        std::wstring getWarningMessage();
    };


//class representing a PRR rule variable
class RuleInterface;
class RuleVariable
    {
    std::map<std::wstring, RuleInterface*> m_variables;

    public:
        RuleVariable(){};
        virtual RULEENGINEEXPORT ~RuleVariable();

        void RULEENGINEEXPORT add(std::wstring name, RuleInterface* pRuleInterface);
        RuleInterface* get(std::wstring name);
    };


//class representing a PRR rule set
class RuleSet
    {
    public:
        struct RULE
        {
            std::wstring ruleText;
            Rule *rule;
            
            ~RULE();
        };
        //typedef std::pair<std::wstring, Rule*> RULE;
        RuleSet(){};
        virtual RULEENGINEEXPORT ~RuleSet();

        int RULEENGINEEXPORT size();
        void RULEENGINEEXPORT addrule(RULE *pRule);
        void RULEENGINEEXPORT getrulestatements(std::vector<std::wstring>& rulestatements);
        void RULEENGINEEXPORT evaluate(std::wstringstream& ostr);

    private:
        std::vector<RULE*> m_ruleset;
    };


//singleton class for PRR rule context
class RuleContext
    {
    static RuleContext* s_rulecontext;
    RuleSet* m_ruleset;
    RuleVariable* m_rulevariable;
        
    RuleContext();

    public:
        static RULEENGINEEXPORT RuleContext* STDCALLCONV getInstance();

        void RULEENGINEEXPORT setruleset(RuleSet* ruleset);
        void RULEENGINEEXPORT setrulevariable(RuleVariable* rulevariable);
        RuleVariable* getrulevariable();

        void RULEENGINEEXPORT evaluate(std::wstringstream& ostr);
    };

}

#endif


