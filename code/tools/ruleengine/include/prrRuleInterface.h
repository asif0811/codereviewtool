//interface representing a rule interface.
//client objects should implement this interface

#ifndef __PRRRULEINTERFACE
#define __PRRRULEINTERFACE

#include <string>
#include <iostream>
#include "oclPrimitiveTypes.h"

namespace PRR
{

class RuleInterface
    {
    public:
        RuleInterface() {};
        virtual ~RuleInterface(){};
        virtual void evaluate(std::wstring propertyname, OCL::Any& propertyvalue) = 0;
        
    protected:
        bool initialized;
        virtual bool initialize() = 0;
        void logError(std::wstring error) { std::wcout << error.c_str() << std::endl; }
        
    };

}

#endif


