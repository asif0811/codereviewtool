//Classes representing primitive data types used in OCL.

#ifndef __OCLPRIMITIVES
#define __OCLPRIMITIVES

#include <string>

#include "oclBase.h"

#define RULEENGINEEXPORT

#ifdef _WIN32
#define RULEENGINEEXPORT  __declspec(dllexport)
#endif

namespace OCL
{

//Class for OCL Real type
class RULEENGINEEXPORT Real : public Type
    {
    double m_value;

    public:
        Real()             : m_value(0.0)   {};
        Real(double value) : m_value(value) {};
        Real(int value)    : m_value(value) {};
        virtual ~Real() {};

        Real operator + (const Real& r);
        Real operator - (const Real& r);
        Real operator * (const Real& r);
        Real operator / (const Real& r);

        Real operator - (); //unary operator. this is for -Real operation. reconsider this. 

        virtual bool operator <  (const Real& r);
        virtual bool operator >  (const Real& r);
        virtual bool operator <= (const Real& r);
        virtual bool operator >= (const Real& r);

        virtual bool operator == (Type& type);
        virtual bool operator != (Type& type);

        friend class Any;
    };


//Class for OCL Integer type
class RULEENGINEEXPORT Integer : public Real
    {
    int m_value;

    public:
        Integer()          : m_value(0)     {};
        Integer(int value) : m_value(value) {};
        virtual ~Integer() {};

        Integer operator + (const Integer& r);
        Integer operator - (const Integer& r);
        Integer operator * (const Integer& r);
        Real    operator / (const Integer& r);

        Integer operator - (); //unary operator. this is for -Integer operation. reconsider this.
        
        virtual bool operator ==  (Type& type);
        virtual bool operator !=  (Type& type);
        int value() const { return m_value; };

        friend class Any;
    };


//Class for OCL SString type
class RULEENGINEEXPORT SString : public Type
    {
    std::wstring m_value;

    public:
        SString()                   : m_value(std::wstring()) {};
        SString(std::wstring value) : m_value(value)          {};
        virtual ~SString() {};

        Integer size();
        SString concat(const SString str);
        SString substring(Integer lower, Integer upper);
        Integer toInteger();
        Real toReal();

        virtual bool operator ==  (Type& type);
        virtual bool operator !=  (Type& type);
        virtual bool startsWith(const SString& str);

        bool operator == (SString& str);
        bool operator != (SString& str);

        friend class Any;
    };


//Class for OCL Boolean type
class RULEENGINEEXPORT Boolean : public Type
    {
    bool m_value;

    public:
        Boolean()           : m_value(false) {};
        Boolean(bool value) : m_value(value) {};
        virtual ~Boolean() {};

        Boolean OR (Boolean b);
        Boolean AND (Boolean b);
        Boolean NOT (Boolean b);

        virtual bool operator ==  (Type& type);
        virtual bool operator !=  (Type& type);

        Boolean operator & (Boolean& boolean);
        Boolean operator | (Boolean& boolean);

        friend class Any;
    };


//class representing OCL ANY type
class RULEENGINEEXPORT Any : public Type
    {
    struct AnyType {
        Real real;
        Integer integer;
        SString string;
        Boolean boolean;       
        AnyType() { real = 0.0; integer = 0; string = std::wstring(); boolean = false; }
        };

    bool m_invalidtype;

    public:
        AnyType m_value;

        Any() : m_invalidtype(false) {};
        Any(const Real& real);
        Any(const Integer& integer);
        Any(const SString& string);
        Any(const Boolean& boolean);
        Any(const Invalid& invalid);
        Any(const Any& any);
        virtual ~Any() {};

        bool IsInvalid()  { return m_invalidtype; };
        bool boolresult() { return m_value.boolean.m_value; };
        std::wstring strresult() { return m_value.string.m_value; };

        Any operator &  (Any& any);
        Any operator |  (Any& any);
        Any operator == (Any& any);
        Any operator != (Any& any);
        Any operator <  (Any& any);
        Any operator <= (Any& any);
        Any operator >  (Any& any);
        Any operator >= (Any& any);
        Any operator +  (Any& any);
        Any operator -  (Any& any);
        Any operator *  (Any& any);
        Any operator /  (Any& any);

        virtual Any oclAsType(Type& type);
        virtual bool oclIsTypeOf(Type& type);
        
	//special operators
	Any startsWith(Any& any);

        virtual bool operator ==  (Type& type) { return false; } //not implemented for now.
        virtual bool operator !=  (Type& type) { return false; } //not implemented for now.
        //virtual std::set<Any> allInstances(){}; //not implemented for now.
        //template<typename T> bool oclIsKindOf<T>(){ return true; }  //not implemented for now.
    };

}

#endif
