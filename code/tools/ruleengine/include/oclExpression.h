//Classes representing OCL expressions

#ifndef __OCLEXPRESSION
#define __OCLEXPRESSION

#include <vector>
#include <stack>

#include "oclExpressionOperators.h"
#include "oclPrimitiveTypes.h"

namespace OCL
{
//class representing base OCL expression
class IfExpression;
class LogicalExpression;
class Expression
    {
    IfExpression* m_ifthenexpression;
    LogicalExpression *m_simpleexpression;
    std::wstring m_errormessage;
    std::wstring m_warningmessage;

    public:
        Expression(){};
        Expression(IfExpression* ifthenexpression);
        Expression(LogicalExpression* simpleexpression);
        virtual ~Expression();

        virtual Any evaluate();
        
        virtual void setErrorMessage(std::wstring message);
        virtual std::wstring getErrorMessage();

        virtual void setWarningMessage(std::wstring message);
        virtual std::wstring getWarningMessage();
        
        bool IsSimpleExpression() { return (m_simpleexpression != NULL); }
    };

//class representing If-then OCL expression
class LogicalExpression;
class IfExpression
    {
    LogicalExpression* m_ifcondition;
    LogicalExpression* m_thenexpression;

    public:
        IfExpression(LogicalExpression* ifcondition, LogicalExpression* thenexpression);
        virtual ~IfExpression();

        virtual Any evaluate();
    };

//class representing logical OCL expression
class RelationalExpression;
class LogicalExpression
    {
    public:
        struct RelationalExpressionStruct
            {
            RelationalExpression* relationalexpression;
            LogicalOperator logicaloperator;

            RelationalExpressionStruct(RelationalExpression* prelationalexpression, LogicalOperator logicaloper = LOG_NONE);
            ~RelationalExpressionStruct();
            };

        LogicalExpression(){};
        virtual ~LogicalExpression();
        
        void add(RelationalExpressionStruct* relationaloperation);
        virtual Any evaluate(bool then_expression = false);

    private:
        std::vector<RelationalExpressionStruct*> m_relationalexpressions;
    };


//class representing relational OCL expression
class AdditiveExpression;
class RelationalExpression
    {
    AdditiveExpression* m_leftadditiveexpression;
    RelationalOperator m_relationaloperator;
    AdditiveExpression* m_rightadditiveexpression;

    public:
        RelationalExpression(AdditiveExpression* leftadditiveexpression, RelationalOperator relationaloperator, AdditiveExpression* rightadditiveexpression);
        virtual ~RelationalExpression();

        virtual Any evaluate(bool then_expression = false);
    };


//class representing additive OCL expression
class MultiplicativeExpression;
class AdditiveExpression
    {
    public:
        struct MultiplicativeExpressionStruct
            {
            MultiplicativeExpression* multiplicativeexpression;
            AdditiveOperator additiveoperator;

            MultiplicativeExpressionStruct(MultiplicativeExpression* pmultexpression, AdditiveOperator addoper = ADDITIVE_NONE);
            ~MultiplicativeExpressionStruct();
            };

        AdditiveExpression(){};
        virtual ~AdditiveExpression();
        
        void add(MultiplicativeExpressionStruct* multiplicativeoperation);
        virtual Any evaluate();

    private:
        std::vector<MultiplicativeExpressionStruct*> m_multiplicativeexpressions;
    };


//class representing multiplicative OCL expression
class UnaryExpression;
class MultiplicativeExpression
    {
    public:
        struct UnaryExpressionStruct
            {
            UnaryExpression* unaryexpression;
            MultiplicativeOperator multiplicativeoperator;

            UnaryExpressionStruct(UnaryExpression* punaryexpression, MultiplicativeOperator multoper = MULTIPLICATIVE_NONE);
            ~UnaryExpressionStruct();
            };

        MultiplicativeExpression(){};
        virtual ~MultiplicativeExpression();
        
        void add(UnaryExpressionStruct* unaryoperation);
        virtual Any evaluate();

    private:
        std::vector<UnaryExpressionStruct*> m_unaryexpressions;
    };


//class representing unary OCL expression
class PostfixExpression;
class UnaryExpression
    {
    PostfixExpression* m_postfixexpression;
    
    public:
        std::stack<UnaryOperator> m_unaryoperators;

        UnaryExpression(){};
        virtual ~UnaryExpression();
        
        void add(PostfixExpression* postfixexpression);
        virtual Any evaluate();
    };

//class representing postfix OCL expression
class PrimaryExpression;
class PostfixExpression
    {
    PrimaryExpression* m_primaryexpression;
    //will implement rest of the postfix expression structure later
    
    public:
        PostfixExpression(PrimaryExpression* primaryexpression);
        virtual ~PostfixExpression();

        virtual Any evaluate();
    };


//class representing primary OCL expression
class Literal;
class FeatureCall;
class PrimaryExpression
    {
    public:
        //will implement the complete structure later. minimal implementation for now...
        struct PrimaryExpressionStruct
            {
            Literal* oclliteral;
            FeatureCall* featurecall;
            };

        PrimaryExpression();
        virtual ~PrimaryExpression();
        
        void add(Literal* pliteral);
        void add(FeatureCall* pfeaturecall);
        virtual Any evaluate();

    private:
        PrimaryExpressionStruct m_primaryexpressionstruct;
    };


//class representing literal OCL expression
class Literal
    {      
    public:
        enum Type { OCLSTRING, OCLNUMBER, OCLHASHNAME };

        Literal(std::wstring value, Type type);
        virtual ~Literal(){};

        virtual Any evaluate();

    private:
        Type m_type;
        std::wstring m_strval;
        long m_numval;
        std::wstring m_hashname;
    };

//class representing a feature call
class FeatureCall
    {
    std::wstring m_featurename;
    std::wstring m_methodname;

    public:
        FeatureCall(std::wstring featurename, std::wstring methodname);
        virtual ~FeatureCall(){};

        virtual Any evaluate();
    };



}

#endif


