//Classes representing OCL collection types

#ifndef __OCLCOLLECTION
#define __OCLCOLLECTION

#include <vector>

#include "oclBase.h"
#include "oclPrimitiveTypes.h"

namespace OCL
{

//Class representing a sequential container
class SequentialContainer : public Type
    {
    std::vector<Type*> m_container;

    public:
        SequentialContainer(){};
        
        Integer size();
        Boolean isEmpty();
        void add(Type& type);
        Boolean includes(Type& type);
        Boolean includesAll(SequentialContainer& container);
        Boolean excludes(Type& type);
        Boolean excludesAll(SequentialContainer& container);

        virtual bool operator ==  (Type& type) { return false; };
        virtual bool operator !=  (Type& type) { return false; };
    };

}

#endif

