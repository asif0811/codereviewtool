//Classes representing basic OCL types

#ifndef __OCLBASE
#define __OCLBASE

namespace OCL
{
//class representing an OCL type
class Type
    {
    public:
        Type(){};
        virtual ~Type(){};

        virtual bool operator ==  (Type& type) = 0;
        virtual bool operator !=  (Type& type) = 0;
    };


//class representing OCL type Void
class Void : public Type
    {
    public:
        Void(){};
        virtual ~Void(){};

        virtual bool operator ==  (Type& type) { return true;  };
        virtual bool operator !=  (Type& type) { return false; };
    };


//class representing OCL type Invalid
class Invalid : public Type
    {
    public:
        Invalid(){};
        virtual ~Invalid(){};

        virtual bool operator ==  (Type& type) { return false; };
        virtual bool operator !=  (Type& type) { return false; };
    };

}

#endif


