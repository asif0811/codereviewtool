//#pragma once

#ifndef _OCLUTIL
#define _OCLUTIL

#include <string>

namespace OCL
{
    std::wstring toString(std::string pString);
}

#endif
