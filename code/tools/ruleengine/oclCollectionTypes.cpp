//Implementation of OCL collection types.

#include "oclCollectionTypes.h"

namespace OCL
{

//Sequential container implementation
void SequentialContainer::add(Type& type)
    {
    m_container.push_back(&type);
    }

Integer SequentialContainer::size()
    {
    return m_container.size();
    }

Boolean SequentialContainer::isEmpty()
    {
    return m_container.empty();
    }

Boolean SequentialContainer::includes(Type& type)
    {
    bool found = false;
    for (std::vector<Type*>::iterator it = m_container.begin(); it != m_container.end(); ++it)
        {
        Type* object = *it;
        if (object && *object == type)
            {
            found = true;
            break;
            }
        }
    return found;
    }


Boolean SequentialContainer::includesAll(SequentialContainer& container)
    {
    bool found = true;
    for (std::vector<Type*>::iterator it = container.m_container.begin(); it != container.m_container.end(); ++it)
        {
        found = false;
        for (std::vector<Type*>::iterator it1 = m_container.begin(); it1 != m_container.end(); ++it1)
            {
            Type* object1 = *it;
            Type* object2 = *it1;
            if (object1 && object2 && *object1 == *object2)
                {
                found = true;
                break;
                }
            }

        if (!found)
            break;
        }
    return found;
    }


Boolean SequentialContainer::excludes(Type& type)
    {
    bool notfound = true;
    for (std::vector<Type*>::iterator it = m_container.begin(); it != m_container.end(); ++it)
        {
        Type* object = *it;
        if (object && *object == type)
            {
            notfound = false;
            break;
            }
        }
    return notfound;
    }

Boolean SequentialContainer::excludesAll(SequentialContainer& container)
    {
    bool notfound = true;
    for (std::vector<Type*>::iterator it = container.m_container.begin(); it != container.m_container.end(); ++it)
        {
        notfound = true;
        for (std::vector<Type*>::iterator it1 = m_container.begin(); it1 != m_container.end(); ++it1)
            {
            Type* object1 = *it;
            Type* object2 = *it1;
            if (object1 && object2 && *object1 == *object2)
                {
                notfound = false;
                break;
                }
            }

        if (!notfound)
            break;
        }
    return notfound;
    }

}

