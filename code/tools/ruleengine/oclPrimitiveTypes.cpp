//implementation of primitive data types used in OCL.

#include <stdlib.h>

#include "oclBase.h"
#include "oclPrimitiveTypes.h"

namespace OCL
{

//Real implementation
Real Real::operator + (const Real& r)
    {
    return m_value + r.m_value;
    }

Real Real::operator - (const Real& r)
    {
    return m_value - r.m_value; 
    }

Real Real::operator * (const Real& r)
    {
    return m_value * r.m_value; 
    }

Real Real::operator / (const Real& r)
    {
    if(r.m_value == 0)
          //return Invalid(); ideally Invalid should be returned, need to work on this one
          return 0;
    return m_value / r.m_value;
    } 

Real Real::operator - ()
    { 
    return -m_value; 
    }

bool Real::operator < (const Real& r) 
    {
    return m_value < r.m_value;
    }

bool Real::operator > (const Real& r)
    {
    return m_value > r.m_value; 
    }
 
bool Real::operator <= (const Real& r)
    {
    return m_value <= r.m_value; 
    }

bool Real::operator >= (const Real& r)
    {
    return m_value >= r.m_value; 
    }

bool Real::operator == (Type& type)
    {
    bool result = false;
    Real* real = dynamic_cast<Real*>(&type);
    if (real)
        result =  m_value == real->m_value;
    return result;
    }

bool Real::operator != (Type& type)
    {
    bool result = false;
    Real* real = dynamic_cast<Real*>(&type);
    if (real)
        result =  m_value != real->m_value;
    return result;
    }


//Integer implementation
Integer Integer::operator + (const Integer& r)
    {
    return m_value + r.m_value; 
    }

Integer Integer::operator - (const Integer& r)
    {
    return m_value - r.m_value; 
    }

Integer Integer::operator * (const Integer& r)
    {
    return m_value * r.m_value; 
    }

Real Integer::operator / (const Integer& r)
    {
    if(r.m_value == 0)
        //return Invalid(); ideally Invalid should be returned, need to work on this one
        return 0;

    return Real(m_value / r.m_value);
    }

Integer Integer::operator - ()
    {
    return -m_value; 
    }

bool Integer::operator == (Type& type)
    {
    bool result = false;
    Integer* integer = dynamic_cast<Integer*>(&type);
    if (integer)
        result =  m_value == integer->m_value;
    return result;
    }

bool Integer::operator != (Type& type)
    {
    bool result = false;
    Integer* integer = dynamic_cast<Integer*>(&type);
    if (integer)
        result =  m_value != integer->m_value;
    return result;
    }



//SString implementation
Integer SString::size()
    {
    return m_value.length(); 
    }

SString SString::concat(const SString str)
    {
    return m_value + str.m_value; 
    }

SString SString::substring(Integer lower, Integer upper)
    {
    SString result;
    try
        {
        lower = lower + 1;
        upper = upper - 1;
        result = SString(m_value.substr(lower.value(),upper.value()));
        }
    catch(...)
        {
        result = SString(std::wstring());
        }
    return result;
    }

Integer SString::toInteger()
    {
    return Integer(wcstol(m_value.c_str(),NULL,10));
    }

Real SString::toReal()
    {
    return Real(wcstod(m_value.c_str(),NULL));
    }

bool SString::operator == (Type& type)
    {
    bool result = false;
    SString* string = dynamic_cast<SString*>(&type);
    if (string)
        result =  m_value == string->m_value;
    return result;
    }

bool SString::operator != (Type& type)
    {
    bool result = false;
    SString* string = dynamic_cast<SString*>(&type);
    if (string)
        result =  m_value != string->m_value;
    return result;
    }

bool SString::operator == (SString& str)
    {
    return m_value == str.m_value;
    }

bool SString::operator != (SString& str)
    {
    return m_value != str.m_value;
    }

bool SString::startsWith(const SString& str)
    {
    bool result = false;
    if (m_value.find(str.m_value) == 0)
	result = true;	
    
    return result;
    }

//Boolean implementation
Boolean Boolean::OR(Boolean b)
    {
    return m_value || b.m_value; 
    }

Boolean Boolean::AND(Boolean b)
    {
    return m_value && b.m_value; 
    }

Boolean Boolean::NOT(Boolean b)
    {
    return !m_value; 
    }

bool Boolean::operator == (Type& type)
    {
    bool result = false;
    Boolean* boolean = dynamic_cast<Boolean*>(&type);
    if (boolean)
        result =  m_value == boolean->m_value;
    return result;
    }

bool Boolean::operator != (Type& type)
    {
    bool result = false;
    Boolean* boolean = dynamic_cast<Boolean*>(&type);
    if (boolean)
        result =  m_value != boolean->m_value;
    return result;
    }

Boolean Boolean::operator & (Boolean& boolean)
    {
    return Boolean(m_value & boolean.m_value);
    }

Boolean Boolean::operator | (Boolean& boolean)
    {
    return Boolean(m_value | boolean.m_value);
    }


//Any implementation
Any::Any(const Real& real) : m_invalidtype(false)
    {
    m_value.real = real;
    }

Any::Any(const Integer& integer) : m_invalidtype(false)
    {
    m_value.integer = integer;
    }

Any::Any(const SString& string) : m_invalidtype(false)
    {
    m_value.string = string;
    }

Any::Any(const Boolean& boolean) : m_invalidtype(false)
    {
    m_value.boolean = boolean;
    }

Any::Any(const Invalid& invalid)
    {
    m_invalidtype = true;
    }

Any::Any(const Any& any)
    {
    m_value.real = any.m_value.real;
    m_value.integer = any.m_value.integer;
    m_value.string = any.m_value.string;
    m_value.boolean = any.m_value.boolean;
    m_invalidtype = any.m_invalidtype;
    }

Any Any::oclAsType(Type& type)
    {
    Any any;
    Real* real = dynamic_cast<Real*>(&type);
    Integer* integer = dynamic_cast<Integer*>(&type);
    SString* string = dynamic_cast<SString*>(&type);
    Boolean* boolean = dynamic_cast<Boolean*>(&type);
    if (real)
        any.m_value.real = *real;
    else if (integer)
        any.m_value.integer = *integer;
    else if (string)
        any.m_value.string = *string;
    else if (boolean)
        any.m_value.boolean = *boolean;

    return any;
    }

bool Any::oclIsTypeOf(Type& type)
    {
    bool result = false;
    if (dynamic_cast<Real*>(&type) && m_value.real.m_value)
        result = true;
    else if (dynamic_cast<Integer*>(&type) && m_value.integer.m_value)
        result = true;
    else if (dynamic_cast<SString*>(&type) && !m_value.string.m_value.empty())
        result = true;
    else if (dynamic_cast<Boolean*>(&type) && m_value.boolean.m_value)
        result = true;

    return result;
    }

Any Any::operator & (Any& any)
    {
    if (IsInvalid() || any.IsInvalid())
        return Invalid();

    return Any(m_value.boolean & any.m_value.boolean);
    }

Any Any::operator | (Any& any)
    {
    if (IsInvalid() || any.IsInvalid())
        return Invalid();

    return Any(m_value.boolean | any.m_value.boolean);
    }

Any Any::operator == (Any& any)
    {
    Any result;
    if (IsInvalid() || any.IsInvalid())
        return Invalid();

    if (m_value.integer == any.m_value.integer && m_value.real == any.m_value.real && m_value.string == any.m_value.string && m_value.boolean == any.m_value.boolean)
        result.m_value.boolean.m_value = true;
    
    result.m_value.string = m_value.string;
    return result;
    }

Any Any::operator != (Any& any)
    {
    Any result;
    if (IsInvalid() || any.IsInvalid())
        return Invalid();

    if (m_value.integer != any.m_value.integer || m_value.real != any.m_value.real || m_value.string != any.m_value.string || m_value.boolean != any.m_value.boolean)
        result.m_value.boolean.m_value = true;
   
    return result;
    }

Any Any::operator + (Any& any)
    {
    Any result;
    if (IsInvalid() || any.IsInvalid())
        return Invalid();

    result.m_value.real = m_value.real + any.m_value.real;
    return result;
    }

Any Any::operator - (Any& any)
    {
    Any result;
    if (IsInvalid() || any.IsInvalid())
        return Invalid();

    result.m_value.real = m_value.real - any.m_value.real;
    return result;
    }

Any Any::operator * (Any& any)
    {
    Any result;
    if (IsInvalid() || any.IsInvalid())
        return Invalid();

    result.m_value.real = m_value.real * any.m_value.real;
    return result;
    }

Any Any::operator / (Any& any)
    {
    Any result;
    if (IsInvalid() || any.IsInvalid())
        return Invalid();

    result.m_value.real = m_value.real / any.m_value.real;
    return result;
    }

Any Any::operator < (Any& any)
    {
    Any result;
    if (IsInvalid() || any.IsInvalid())
        return Invalid();
    
    if (m_value.real < any.m_value.real)
        result.m_value.boolean.m_value = true;

    return result;
    }

Any Any::operator <= (Any& any)
    {
    Any result;
    if (IsInvalid() || any.IsInvalid())
        return Invalid();

    if (m_value.real <= any.m_value.real)
        result.m_value.boolean.m_value = true;

    return result;
    }

Any Any::operator > (Any& any)
    {
    Any result;
    if (IsInvalid() || any.IsInvalid())
        return Invalid();

    if (m_value.real > any.m_value.real)
        result.m_value.boolean.m_value = true;

    return result;
    }

Any Any::operator >= (Any& any)
    {
    Any result;
    if (IsInvalid() || any.IsInvalid())
        return Invalid();

    if (m_value.real >= any.m_value.real)
        result.m_value.boolean.m_value = true;

    return result;
    }

Any Any::startsWith(Any& any)
    {
    Any result;
    if (IsInvalid() || any.IsInvalid())
	return Invalid();
    
    if (m_value.string.startsWith(any.m_value.string))
	result.m_value.boolean.m_value = true;

    return result;
    }
}

