#include "util.h"

namespace OCL
{
std::wstring toString(std::string pString)
    {
    std::wstring szString = std::wstring(pString.begin(), pString.end());
    return szString;
    }
}
