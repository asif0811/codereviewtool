//Implementation of the PRR rule classes

#include <iostream>

#include "oclExpression.h"
#include "prrRuleInterface.h"
#include "prrRule.h"

namespace PRR
{

//implementation of Rule
Rule::Rule(OCL::Expression* expression)
    {
    m_expression = expression;
    }

Rule::~Rule()
    {
    if (m_expression)
        delete m_expression;
    }

Rule::EVAL_RESULT Rule::evaluate()
    {
    Rule::EVAL_RESULT bresult = FAIL;
    if (!m_expression)
        return bresult;

    OCL::Any result = m_expression->evaluate();
    bresult = result.boolresult()? PASS : FAIL;
    if (result.IsInvalid())
        bresult = NOEXECUTE;
    //special handling for simple expressions
    //if (m_expression->IsSimpleExpression() && result.strresult().empty())
    //    bresult = NOEXECUTE;
    
    return bresult;
    }

std::wstring Rule::getErrorMessage()
    {
    std::wstring errorMessage;
    if (m_expression)
        errorMessage = m_expression->getErrorMessage();

    return errorMessage;
    }

std::wstring Rule::getWarningMessage()
    {
    std::wstring warningMessage;
    if (m_expression)
        warningMessage = m_expression->getWarningMessage();

    return warningMessage;
    }

//implementation of RuleVariable
RULEENGINEEXPORT RuleVariable::~RuleVariable()
    {
        std::map<std::wstring, RuleInterface*>::iterator it = m_variables.begin();
        while(it != m_variables.end())
        {
            RuleInterface *pRuleInterface = it->second;
            ++it;
            if (pRuleInterface != NULL)
                delete pRuleInterface;
        }
    }

void RULEENGINEEXPORT RuleVariable::add(std::wstring name, RuleInterface* pRuleInterface)
    {
    m_variables[name] = pRuleInterface;
    }

RuleInterface* RuleVariable::get(std::wstring name)
    {
    RuleInterface* rulevariable = NULL;
    if (m_variables.find(name) != m_variables.end())
        rulevariable = m_variables[name];

    return rulevariable;
    }


//implementation of RuleSet
RULEENGINEEXPORT RuleSet::~RuleSet()
    {
        std::vector<RULE*>::iterator it = m_ruleset.begin();
        while(it != m_ruleset.end())
        {
            RULE *pRule = *it;
            ++it;
            if (pRule != NULL)
                delete pRule;
        }
    }

RuleSet::RULE::~RULE()
    {
        if (rule)
            delete rule;
    }

void RULEENGINEEXPORT RuleSet::addrule(RULE *pRule)
    {
    if (pRule && pRule->rule != NULL)
        m_ruleset.push_back(pRule);
    }

int RULEENGINEEXPORT RuleSet::size()
    {
    return m_ruleset.size();
    }

void RULEENGINEEXPORT RuleSet::getrulestatements(std::vector<std::wstring>& rulestatements)
    {
    for (std::vector<RULE*>::iterator it = m_ruleset.begin(); it != m_ruleset.end(); ++it)
        {
        rulestatements.push_back((*it)->ruleText);
        }
    }

void RULEENGINEEXPORT RuleSet::evaluate(std::wstringstream& ostr)
    {
    int count = 1;
    for(std::vector<RULE*>::iterator it = m_ruleset.begin(); it != m_ruleset.end(); ++it, count++)
        {
        Rule *rule = (*it)->rule;
        Rule::EVAL_RESULT result = rule->evaluate();
        std::wstring ruleresult = L"failed...";
        if (result == Rule::PASS || result == Rule::NOEXECUTE)
            ruleresult = L"passed...";
        //else if (result == Rule::NOEXECUTE)
        //    ruleresult = L"skipped. condition not true or invalid property in rule...";

        std::wstring ruleErrorMessage = rule->getErrorMessage();
        std::wstring ruleWarningMessage = rule->getWarningMessage();
        
        if (ostr.good())
            ostr << ((*it)->ruleText).c_str() << " : " << ruleresult.c_str();
        if (result == Rule::FAIL && !ruleErrorMessage.empty())
            ostr << ". Error : " << ruleErrorMessage.c_str();
        else if (result == Rule::PASS && !ruleWarningMessage.empty())
            ostr << ". Warning : " << ruleWarningMessage.c_str();
        ostr << "\r\n";
        }
    }

//implementation of RuleContext
RuleContext* RuleContext::s_rulecontext = NULL;
    
RuleContext::RuleContext()
    {
    }
    
RuleContext* RuleContext::getInstance()
    {
    if (s_rulecontext == NULL)
        s_rulecontext = new RuleContext();

    return s_rulecontext;
    }

void RULEENGINEEXPORT RuleContext::setruleset(RuleSet* ruleset)
    {
    m_ruleset = ruleset;
    }

void RULEENGINEEXPORT RuleContext::setrulevariable(RuleVariable* rulevariable)
    {
    m_rulevariable = rulevariable;
    }

RuleVariable* RuleContext::getrulevariable()
    {
    return m_rulevariable;
    }

void RULEENGINEEXPORT RuleContext::evaluate(std::wstringstream& ostr)
    {
        if(m_ruleset)
            return m_ruleset->evaluate(ostr);
    }

}
