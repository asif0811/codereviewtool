#include "crclassmembers.h"
#include "crtype.h"
#include "crtemplate.h"
#include "crclass.h"
#include "crstatement.h"
#include "crfunction.h"

namespace CodeReview
{

CClassMember::CClassMember()
	{
    accessSpecifier.specifier = AccessSpecifier::SPECIFIER_PRIVATE;
	statementBlock = NULL;
	}

CClassMember::~CClassMember()
	{
	if (statementBlock != NULL)
		delete statementBlock;
	}

CClassMembers::CClassMembers()
	{}

void CClassMembers::addClassMember(CClassMember *classMember, AccessSpecifier accessSpecifier)
    {
    if (classMember != NULL)
    {
	classMember->accessSpecifier.specifier = accessSpecifier.specifier;
    classMembers.push_back(classMember);
	}
    }

CClassMembers::~CClassMembers()
	{
    std::vector<CClassMember*>::iterator it = classMembers.begin();
    while(it != classMembers.end())
    {
        CClassMember *classMember = *it;
        ++it;
        if (classMember != NULL)
            delete classMember;
    }
	}

CClassMethod::CClassMethod()
	{
	name = NULL;
	parameters = NULL;
	}

CClassMethod::CClassMethod(CNestedType *name, CFunctionParameters *parameters)
	{
	this->name = name;
    this->parameters = parameters;
	}

CClassMethod::~CClassMethod()
	{
	if (name != NULL)
		delete name;

	if (parameters != NULL)
		delete parameters;
	}

CClassMemberFriend::CClassMemberFriend()
	{
	name = NULL;
	classTemplate = NULL;
	}

CClassMemberFriend::~CClassMemberFriend()
	{
	if (name != NULL)
		delete name;

	if (classTemplate != NULL)
		delete classTemplate;
	}

CClassMemberNestedClass::CClassMemberNestedClass(CClass *nestedClass)
	{
	this->nestedClass = nestedClass;
	}

CClassMemberNestedClass::~CClassMemberNestedClass()
	{
	if (nestedClass != NULL)
		delete nestedClass;
	}

CClassMemberMethod::CClassMemberMethod()
	{
	bVirtual = false;
	bFriend = false;
	bPureVirtual = false;
	bConstFunction = false;
	bInline = false;
    bStatic = false;
	templateSpec = NULL;
	returnType = NULL;
	method = NULL;
	}

CClassMemberMethod::~CClassMemberMethod()
	{
	if (templateSpec != NULL)
		delete templateSpec;

	if (returnType != NULL)
		delete returnType;

	if (method != NULL)
		delete method;
	}

CClassMemberData::CClassMemberData()
	{
	enumStatement = NULL;
	type = NULL;
	memberArray = NULL;
	memberMethod = NULL;
	}

CClassMemberData::~CClassMemberData()
	{
	if (enumStatement != NULL)
		delete enumStatement;

	if (type != NULL)
		delete type;

	if (memberArray != NULL)
		delete memberArray;

	if (memberMethod != NULL)
		delete memberMethod;
	}

CClassMemberStatic::CClassMemberStatic()
	{
	type = NULL;
	member = NULL;
	}

CClassMemberStatic::StaticMember::StaticMember()
	{
	arrayMember = NULL;
	methodMember = NULL;
	}

CClassMemberStatic::StaticMember::~StaticMember()
	{
	if (arrayMember != NULL)
		delete arrayMember;

	if (methodMember != NULL)
		delete methodMember;
	}

CClassMemberStatic::~CClassMemberStatic()
	{
	if (type != NULL)
		delete type;

	if (member != NULL)
		delete member;
	}

CClassMemberConstructor::CClassMemberConstructor()
	{
   	bCopyConstructor = false;
	bexplicitSpecifier = false;
	parameters = NULL;
	}

CClassMemberConstructor::~CClassMemberConstructor()
	{
	if (parameters != NULL)
		delete parameters;

    std::vector<CFunctionCall*>::iterator it = baseClassConstructors.begin();
    while(it != baseClassConstructors.end())
    {
        CFunctionCall *baseConstructor = *it;
        ++it;
        if (baseConstructor != NULL)
            delete baseConstructor;
    }
	}

CClassMemberDestructor::CClassMemberDestructor()
	{
	bVirtual = false;
	parameters = NULL;
	}

CClassMemberDestructor::~CClassMemberDestructor()
	{
	if (parameters != NULL)
		delete parameters;
	}

}
