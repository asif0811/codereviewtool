#include "util.h"
#include "crfile.h"
#include "crfunction.h"
#include "crstatement.h"
#include "crtype.h"
#include "crclassmembers.h"

namespace CodeReview
{
    
std::wstring toString(std::string pString)
    {
    std::wstring szString = std::wstring(pString.begin(), pString.end());
    return szString;
    }
    
CCodeUnit* addToCodeUnit(CFile *crFile, CFunctionSignature *crFuncSignature, CStatementBlock *crStatementBlock)
    {
        CCodeUnit *codeUnit = NULL;
        CNestedType *functionName = crFuncSignature->name;
        
        //differentiated between simple function and class method
        if (!(functionName->namespace1.empty()) && !(functionName->namespace2.empty()))
        {
            //this is a class method
            //check if the class is already added as a code unit
            bool newClass = false;
            CClass *crClass = NULL;
            std::wstring className = functionName->namespace1;
            std::list<CCodeUnit*>::iterator it = crFile->codeUnits.begin();
            while(it != crFile->codeUnits.end())
            {
                CCodeUnit *fileCodeUnit = *it;
                CClass *codeunitClass = dynamic_cast<CClass*>(fileCodeUnit);
                if (codeunitClass &&  codeunitClass->name.compare(className) == 0) {
                    crClass = codeunitClass;
                    break;
                }
                ++it;
            }
            
            if (crClass == NULL)
            {
                crClass = new CClass(CClass::CRCLASS, className);
                newClass = true;
            }
            
            //creat a new class member method and associate with the class
            CClassMemberMethod *crClassMemberMethod = new CClassMemberMethod();
            CNestedType *methodName = new CNestedType();
            methodName->namespace1 = functionName->namespace2;
            crClassMemberMethod->method = new CClassMethod(methodName, crFuncSignature->parameters);
            crClassMemberMethod->returnType = crFuncSignature->returnType;
            crClassMemberMethod->templateSpec = crFuncSignature->templateSpec;
            AccessSpecifier accessSpecifier;
            if (crClass->classMembers == NULL)
                crClass->classMembers = new CClassMembers();
            crClass->classMembers->addClassMember(crClassMemberMethod, accessSpecifier);
            if (newClass)
                codeUnit = crClass;
        }
        else
        {
            //this is a simple function
            CFunction *crFunction = new CFunction();
            crFunction->signature = crFuncSignature;
            crFunction->statementBlock = crStatementBlock;
            codeUnit = crFunction;
        }
        
        return codeUnit;
    }
    
}
