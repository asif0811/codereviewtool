#include "crexpression.h"
#include "crexpressionbase.h"
#include "crexpression1.h"
#include "crexpression2.h"
#include "crtype.h"

namespace CodeReview
{

CExpression::CExpression() : CExpressionBase()
	{
	lexpression = NULL;
	}

CExpression::~CExpression()
	{
	if (lexpression != NULL)
		delete lexpression;
	
    std::vector<CExpressionBase*>::iterator it = rexpressions.begin();
    while(it != rexpressions.end())
    {
        CExpressionBase *expression = *it;
        ++it;
        if (expression != NULL)
            delete expression;
    }
	}

void CExpression::addRExpression(CExpressionBase *expression)
    {
        if (expression != NULL)
            rexpressions.push_back(expression);
    }

CExpressionLeft::CExpressionLeft() : CExpressionBase()
	{
	type = NULL;
	expression2 = NULL;
	bStatic = false;
	expression = NULL;
	}

CExpressionLeft::~CExpressionLeft()
	{
	if (type != NULL)
		delete type;

	if (expression2 != NULL)
		delete expression2;

	if (expression != NULL)
		delete expression;
	}
}
