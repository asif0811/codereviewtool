
#include "crtype.h"
#include "crtemplate.h"
#include "crexpression.h"
#include "crstatement.h"
#include "crfunction.h"

#include <iostream>

namespace CodeReview
{

CFunction::CFunction()
	{
	signature = NULL;
	statementBlock = NULL;
	}

CFunction::~CFunction()
	{
	if(signature != NULL)
		delete signature;
	
	if (statementBlock != NULL)
		delete statementBlock;
	}

std::wstring CFunction::getType()
    {
        return std::wstring(L"Function");
    }
    
std::wstring CFunction::getInfo()
    {
        std::wstring info = L"Function name: ";
        if (signature && signature->name) {
            std::wstring namespace1 = signature->name->namespace1;
            std::wstring namespace2 = signature->name->namespace2;
            std::wstring namespace3 = signature->name->namespace3;
            std::wstring functionName = namespace1;
            if(!namespace1.empty() && !namespace2.empty() && !namespace3.empty())
                functionName  = namespace1 + L"::" + namespace2 + L"::" + namespace3;
            else if(!namespace1.empty() && !namespace2.empty())
                functionName  = namespace1 + L"::" + namespace2;
            info = info + functionName;
        }
        return info;
    }

void CFunction::getChildCodeUnits(std::list<CCodeUnit*>& childCodeUnits)
    {
    }

CFunctionSignature::CFunctionSignature()
	{
	name = NULL;
	parameters = NULL;
	returnType = NULL;
	exportType = NULL;
	templateSpec = NULL;
	
	bjniCall = binline = bconstFunction = bvirtual = bclassDestructor = false;
	}

CFunctionSignature::~CFunctionSignature()
	{
	if (name != NULL)
		delete name;

	if (parameters != NULL)
		delete parameters;

	if (returnType != NULL)
		delete returnType;

	if (exportType != NULL)
		delete exportType;

	if (templateSpec != NULL)
		delete templateSpec;
	}

CFunctionParameter::CFunctionParameter()
	{
	type = NULL;
	name = NULL;
	defaultValue = NULL;
	}

CFunctionParameter::~CFunctionParameter()
	{
	if (type != NULL)
		delete type;
        
	if (name != NULL)
		delete name;
        
	if (defaultValue != NULL)
		delete defaultValue;
	}

CFunctionParameters::CFunctionParameters()
    {}

CFunctionParameters::~CFunctionParameters()
	{
    std::vector<CFunctionParameter*>::iterator it = parameters.begin();
    while(it != parameters.end())
    {
        CFunctionParameter *funcParameter = *it;
        ++it;
        if (funcParameter != NULL)
            delete funcParameter;
    }
	}
    
void CFunctionParameters::addParameter(CFunctionParameter *parameter)
    {
    if (parameter != NULL)
        parameters.push_back(parameter);
    }
    
std::vector<CFunctionParameter*>& CFunctionParameters::getParameters()
    {
    return parameters;
    }
    
void CFunctionParameters::getParametersCount(int &parametersCount, int &defaultParametersCount)
    {
        parametersCount = parameters.size();
        defaultParametersCount = 0;
        for (std::vector<CFunctionParameter*>::iterator it = parameters.begin(); it != parameters.end(); ++it)
        {
            CFunctionParameter *parameter = *it;
            if (parameter && parameter->defaultValue)
                defaultParametersCount++;
        }
    }

CFunctionArguments::CFunctionArguments()
	{}

CFunctionArguments::~CFunctionArguments()
	{
    std::vector<CExpression*>::iterator it = arguments.begin();
    while(it != arguments.end())
    {
        CExpression *expression = *it;
        ++it;
        if (expression != NULL)
            delete expression;
    }
	}
    
void CFunctionArguments::addArgument(CExpression *argument)
    {
    if (argument != NULL)
        arguments.push_back(argument);
    }
    
std::vector<CExpression*>& CFunctionArguments::getArguments()
    {
    return arguments;
    }

CFunctionCall::CFunctionCall()
	{
	nestedType = NULL;
	arguments = NULL;
	}

CFunctionCall::~CFunctionCall()
	{
	if (nestedType != NULL)
		delete nestedType;
	
	if (arguments != NULL)
		delete arguments;
	}

}
