#include <cstddef>
#include "crstatement.h"
#include "crexpression.h"
#include "crtype.h"

namespace CodeReview
{

CStatementBlock::CStatementBlock()
	{}

CStatementBlock::CStatementBlock(CStatement *statement)
	{
	if (statement != NULL)
		statements.push_back(statement);
	}
    
void CStatementBlock::addStatement(CStatement *statement)
    {
        if (statement != NULL)
            statements.push_back(statement);
    }

CStatementBlock::~CStatementBlock()
	{
    std::vector<CStatement*>::iterator it = statements.begin();
    while(it != statements.end())
    {
        CStatement *statement = *it;
        ++it;
        if (statement != NULL)
            delete statement;
    }
	}

CStatementLR::CStatementLR()
	{
	lexpression = NULL;
	rexpression = NULL;
	arrayInitializer = NULL;
	lrOperator = EQUAL;
	}

CStatementLR::~CStatementLR()
	{
	if (lexpression != NULL)
		delete lexpression;

	if (rexpression != NULL)
		delete rexpression;

	if (arrayInitializer != NULL)
		delete arrayInitializer;

    std::vector<CStatementLR*>::iterator it = siblingLRStatements.begin();
    while(it != siblingLRStatements.end())
    {
        CStatementLR *statement = *it;
        ++it;
        if (statement != NULL)
            delete statement;
    }
	}

CStatementCondition::CStatementCondition()
	{
	condition_lr = NULL;
	condition_exp = NULL;
	statement_pass = NULL;
	statement_fail = NULL;
	}

CStatementCondition::~CStatementCondition()
	{
	if (condition_lr != NULL)
		delete condition_lr;

	if (condition_exp != NULL)
		delete condition_exp;

	if (statement_pass != NULL)
		delete statement_pass;

	if (statement_fail != NULL)
		delete statement_fail;
	}

CStatementEnum::CStatementEnum()
	{}

CStatementEnum::EnumItem::EnumItem()
	{
	name = NULL;
	value = NULL;
	}

CStatementEnum::EnumItem::~EnumItem()
	{
	if (name != NULL)
		delete name;

	if (value != NULL)
		delete value;
	}

CStatementEnum::~CStatementEnum()
	{
    std::vector<EnumItem*>::iterator it = enumItems.begin();
    while(it != enumItems.end())
    {
        EnumItem *enumItem = *it;
        ++it;
        if (enumItem != NULL)
            delete enumItem;
    }
    }

CStatementReturn::CStatementReturn()
	{
	expression = NULL;
	}

CStatementReturn::~CStatementReturn()
	{
	if (expression != NULL)
		delete expression;
	}

CStatementIf::CStatementIf()
	{
	condition = NULL;
	actionBlock = NULL;
	}

CStatementIf::~CStatementIf()
	{
	if (condition != NULL)
		delete condition;

	if (actionBlock != NULL)
		delete actionBlock;
	}

CStatementElse::CStatementElse()
	{
	statementBlock = NULL;
	}

CStatementElse::~CStatementElse()
	{
	if (statementBlock != NULL)
		delete statementBlock;
	}

CStatementFor::CStatementFor()
	{
	startStatement = NULL;
	endExpression = NULL;
	loopExpression = NULL;
	statementBlock = NULL;
	}

CStatementFor::~CStatementFor()
	{
	if (startStatement != NULL)
		delete startStatement;

	if (endExpression != NULL)
		delete endExpression;

	if (loopExpression != NULL)
		delete loopExpression;

	if (statementBlock != NULL)
		delete statementBlock;
	}

CStatementWhile::CStatementWhile()
	{
	condition = NULL;
	statementBlock = NULL;
	}

CStatementWhile::~CStatementWhile()
	{
	if (condition != NULL)
		delete condition;

	if (statementBlock != NULL)
		delete statementBlock;
	}

CStatementTryCatch::CStatementTryCatch()
	{
	tryBlock = NULL;
	catchBlock = NULL;	
	}

CStatementTryCatch::~CStatementTryCatch()
	{
	if (tryBlock != NULL)
		delete tryBlock;

	if (catchBlock != NULL)
		delete catchBlock;
	}

CCaseStatement::CCaseStatement()
	{
	condition = NULL;
	conditionActionBlock = NULL;
	defaultActionBlock = NULL;
	}

CCaseStatement::~CCaseStatement()
	{
	if (condition != NULL)
		delete condition;

	if (conditionActionBlock != NULL)
		delete conditionActionBlock;

	if (defaultActionBlock != NULL)
		delete defaultActionBlock;
	}

CStatementSwitchCase::CStatementSwitchCase()
	{
	switchExpression = NULL;
	}

CStatementSwitchCase::~CStatementSwitchCase()
	{
	if (switchExpression != NULL)
		delete switchExpression;

    std::vector<CCaseStatement*>::iterator it = caseStatements.begin();
    while(it != caseStatements.end())
    {
        CCaseStatement *statement = *it;
        ++it;
        if (statement != NULL)
            delete statement;
    }
	}
}
