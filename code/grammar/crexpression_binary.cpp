#include "crexpression_binary.h"
#include "crexpression.h"
#include "crexpression1.h"
#include "crexpression2.h"
#include "croperator.h"

namespace CodeReview
{

CExpressionBinary::CExpressionBinaryLeft::CExpressionBinaryLeft()
	{
	expression = NULL;
	expression2 = NULL;
	}

CExpressionBinary::CExpressionBinaryLeft::~CExpressionBinaryLeft()
	{
	if (expression != NULL)
		delete expression;

	if (expression2 != NULL)
		delete expression2;
	}

CExpressionBinary::CExpressionBinary()
	{
	lexpression = NULL;
	binaryOp = NULL;
	rexpression = NULL;
	}

CExpressionBinary::~CExpressionBinary()
	{
	if (lexpression != NULL)
		delete lexpression;

	if (binaryOp != NULL)
		delete binaryOp;

	if (rexpression != NULL)
		delete rexpression;
	}

}
