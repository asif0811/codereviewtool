#include "crexpression2.h"
#include "crtype.h"
#include "crfunction.h"

namespace CodeReview
{

CExpression2::CExpression2() : CExpressionBase()
	{
	bnumberZero = false;
	bnumber = false;
	bconstString = false;

	identifier = NULL;
	functionCall = NULL;
	arrayType = NULL;	
	}

CExpression2::~CExpression2()
	{
	if (identifier != NULL)
		delete identifier;

	if (functionCall != NULL)
		delete functionCall;

	if (arrayType != NULL)
		delete arrayType;
	}

}

