#include "crtype.h"
#include "crtemplate.h"

namespace CodeReview
{

CTemplateDeclaration::CTemplateDeclaration()
	{
	nestedType = NULL;
	}

CTemplateDeclaration::~CTemplateDeclaration()
	{
	if (nestedType != NULL)
		delete nestedType;
	}

CTemplateSpec::CTemplateSpec()
	{}

CTemplateSpec::~CTemplateSpec()
	{
    std::vector<CTemplateDeclaration*>::iterator it = templateDeclarations.begin();
    while(it != templateDeclarations.end())
    {
        CTemplateDeclaration *templateDeclaration = *it;
        ++it;
        if (templateDeclaration != NULL)
            delete templateDeclaration;
    }
	}	

CClassTemplate::CClassTemplate()
	{
	type = NULL;
	btypeName = false;
	}

CClassTemplate::~CClassTemplate()
	{
	if (type != NULL)
		delete type;
        
    std::vector<CClassTemplate*>::iterator it = nestedTemplates.begin();
    while(it != nestedTemplates.end())
    {
        CClassTemplate *nestedTemplate = *it;
        ++it;
        if (nestedTemplate != NULL)
            delete nestedTemplate;
    }
	}
}
