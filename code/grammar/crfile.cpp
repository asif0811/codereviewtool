#include "crfile.h"

namespace CodeReview
{

CFile::CFile()
	{}

CFile::~CFile()
	{
    std::list<CCodeUnit*>::iterator it = codeUnits.begin();
    while(it != codeUnits.end())
    {
        CCodeUnit *codeUnit = *it;
        ++it;
        if (codeUnit != NULL)
            delete codeUnit;
    }
	}

void CFile::addCodeUnit(CCodeUnit *codeUnit)
	{
	if (codeUnit != NULL)
		codeUnits.push_back(codeUnit);
	}

CCodeUnit::CCodeUnit() : lineNumber(0)
	{}

CCodeUnit::~CCodeUnit()
	{}
    
std::wstring CCodeUnit::getType()
    {
    return std::wstring(L"CodeUnit");
    }

std::wstring CCodeUnit::getInfo()
	{
	return L"";
	}

int CCodeUnit::getLine()
    {
    return lineNumber;
    }
    
void CCodeUnit::setLine(int lineNumber)
    {
    this->lineNumber = lineNumber;
    }

void CCodeUnit::getChildCodeUnits(std::list<CCodeUnit*>& childCodeUnits)
    {}
}
