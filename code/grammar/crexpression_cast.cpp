#include "crexpression_cast.h"
#include "crtype.h"
#include "crexpression1.h"

namespace CodeReview
{

CExpressionCast::CExpressionCast()
	{
	type = NULL;
	expression = NULL;
	}

CExpressionCast::~CExpressionCast()
	{
	if (type != NULL)
		delete type;

	if (expression != NULL)
		delete expression;
	}

}
