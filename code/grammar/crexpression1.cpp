#include <cstddef>

#include "crexpression.h"
#include "crexpression2.h"
#include "croperator.h"
#include "crexpression_binary.h"

#include "crexpression1.h"


namespace CodeReview
{

CExpression1::CExpression1() : CExpressionBase()
    {
        expression = NULL;
    }
    
CExpression1::CExpression1(CExpressionBase *expression) : CExpressionBase()
	{
	this->expression = expression;
	}
    
void CExpression1::addBinaryExpression(CExpression *left_expression1, CExpression2 *left_expression2, COperator *binary_operator, CExpression1 *right_expression)
    {
        if ((left_expression1 != NULL || left_expression2 != NULL) && right_expression != NULL && binary_operator != NULL)
        {
            CExpressionBinary *binaryExpression = new CExpressionBinary();
            binaryExpression->lexpression = new CExpressionBinary::CExpressionBinaryLeft();
            if (left_expression1 != NULL)
                binaryExpression->lexpression->expression = left_expression1;
            else
                binaryExpression->lexpression->expression2 = left_expression2;
            
            binaryExpression->rexpression = right_expression;
            binaryExpression->binaryOp = binary_operator;
            this->expression = binaryExpression;
        }
        else if (left_expression1 != NULL)
            this->expression = left_expression1;
        else
            this->expression = left_expression2;
    }

CExpression1::~CExpression1()
	{
	if (expression != NULL)
		{
		delete expression;
        expression = NULL;
		}
	}
}
