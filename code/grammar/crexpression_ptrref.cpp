#include <cstddef>

#include "crexpression_ptrref.h"
#include "crexpression1.h"

namespace CodeReview
{

CExpressionPtrRef::CExpressionPtrRef()
	{
	expression = NULL;
	bpointer = false;
	breference = false;
	}

CExpressionPtrRef::~CExpressionPtrRef()
	{
	if (expression != NULL)
		delete expression;
	}
}
