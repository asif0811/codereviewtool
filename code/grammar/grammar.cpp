#include "creviewLexer.hpp"
#include "creviewParser.hpp"


namespace CodeReview
{

CFile* parseCode(std::string& fileName, std::wstring& errorString)
    {
    CFile* codeFile = NULL;
    ANTLR_UINT8* fName = (ANTLR_UINT8*)fileName.c_str();

    try
        {
            CodeReviewGrammar::creviewLexer::InputStreamType input(fName, ANTLR_ENC_8BIT);
            CodeReviewGrammar::creviewLexer lxr(&input);
            CodeReviewGrammar::creviewParser::TokenStreamType tstream(ANTLR_SIZE_HINT, lxr.get_tokSource() );
            CodeReviewGrammar::creviewParser psr(&tstream);
            
            codeFile = psr.rulestart();
            if (codeFile == NULL)   
 	        {
                errorString = L"Error while parsing the source code";
            }
        }
    catch(...)
        {
        codeFile = NULL;
        errorString = L"Error while parsing the source code : exception thrown while executing the parser";
        }

    return codeFile;
   }

} 
