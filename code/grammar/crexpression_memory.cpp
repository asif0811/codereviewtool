#include <cstddef>

#include "crexpression_memory.h"
#include "croperator.h"
#include "crexpression1.h"

namespace CodeReview
{

CExpressionMemory::CExpressionMemory()
	{
	memOp = NULL;
	expression = NULL;
	}

CExpressionMemory::~CExpressionMemory()
	{
	if (memOp != NULL)
		delete memOp;

	if (expression != NULL)
		delete expression;
	}

}
