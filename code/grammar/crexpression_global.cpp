#include <cstddef>

#include "crexpression_global.h"
#include "crexpression2.h"

namespace CodeReview
{

CExpressionGlobal::CExpressionGlobal()
	{
	expression = NULL;
	}

CExpressionGlobal::~CExpressionGlobal()
	{
	if (expression != NULL)
		delete expression;
	}

}

