#include "crexpression_dyncast.h"
#include "crtemplate.h"
#include "crexpression1.h"

namespace CodeReview
{

CExpressionDynCast::CExpressionDynCast()
	{
	classTemplate = NULL;
	expression = NULL;
	}

CExpressionDynCast::~CExpressionDynCast()
	{
	if (classTemplate != NULL)
		delete classTemplate;

	if (expression != NULL)
		delete expression;
	}

}
