#include <cstddef>

#include "crexpression_unary.h"
#include "croperator.h"
#include "crexpression1.h"
#include "crexpression2.h"

namespace CodeReview
{

CExpressionUnaryPre::CExpressionUnaryPre()
	{
	unaryOp = NULL;
	expression = NULL;
	bnegative = false;
	bnot = false;
	}

CExpressionUnaryPre::~CExpressionUnaryPre()
	{
	if (unaryOp != NULL)
		delete unaryOp;

	if (expression != NULL)
		delete expression;
	}

CExpressionUnaryPost::CExpressionUnaryPost()
	{
	unaryOp = NULL;
	expression = NULL;
	}

CExpressionUnaryPost::~CExpressionUnaryPost()
	{
	if (unaryOp != NULL)
        delete unaryOp;

    if (expression != NULL)
        delete expression;
	}

}
