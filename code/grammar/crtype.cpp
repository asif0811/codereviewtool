#include "crtemplate.h"
#include "crexpression.h"
#include "crfunction.h"
#include "crtype.h"

namespace CodeReview
{

CNestedType::CNestedType()
	{
	specifier = CNestedType::CRNONE;
	}

CNestedType::~CNestedType()
	{}

CType::CType()
	{
	nestedType = NULL;
	classTemplate = NULL;
	}

CType::~CType()
	{
	if (nestedType != NULL)
		delete nestedType;
        
	if (classTemplate != NULL)
		delete classTemplate;
	}

CExportType::CExportType()
	{
	nestedType = NULL;
	}

CExportType::~CExportType()
	{
	if (nestedType != NULL)
		delete nestedType;
	}

CIdentifier::CIdentifier()
	{
	type = NULL;
    ptrOperator = CIdentifier::CRNONE;
	}

CIdentifier::~CIdentifier()
	{
	if (type != NULL)
		delete type;
	}

CArrayType::CArrayType()
	{
	nestedType = NULL;
	expression = NULL;
	}

CArrayType::~CArrayType()
	{
	if (nestedType != NULL)
		delete nestedType;
        
	if (expression != NULL)
		delete expression;
	}

CArrayInitializer::CArrayInitializer()
	{
	arguments = NULL;
	}

CArrayInitializer::~CArrayInitializer()
	{
	if (arguments != NULL)
		delete arguments;	
	}
}

