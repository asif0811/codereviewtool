#ifndef _CRTEMPLATE
#define _CRTEMPLATE

#include <vector>

namespace CodeReview
{

//class: CTemplateDeclaration

class CNestedType;
class CTemplateDeclaration
	{
	public:
		CNestedType *nestedType;

		CTemplateDeclaration();
		virtual ~CTemplateDeclaration();
	}; 

//class: CTemplateSpec
class CTemplateSpec
	{
	public:
		std::vector<CTemplateDeclaration*> templateDeclarations;

		CTemplateSpec();
		virtual ~CTemplateSpec();
	};

//class: CClassTemplate
class CType;
class CClassTemplate
	{
	public:
		CType *type;
		std::vector<CClassTemplate*> nestedTemplates;
		bool btypeName;

		CClassTemplate();
		virtual ~CClassTemplate();	
	};
}

#endif
