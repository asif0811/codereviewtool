#ifndef _CRCLASSMEMBERS
#define _CRCLASSMEMBERS


#include <string>
#include <vector>

#include "crclass.h"

namespace CodeReview
{

//class: CClassMember - base class for all type of class members
class CStatementBlock;
class CClassMember
	{
	public:
		CClassMember();
		virtual ~CClassMember();

		AccessSpecifier accessSpecifier;
		CStatementBlock *statementBlock;
	};

//class: CClassMembers
class CClassMembers
	{
	public:
		CClassMembers();
		virtual ~CClassMembers();

		std::vector<CClassMember*> classMembers;
        void addClassMember(CClassMember *classMember, AccessSpecifier accessSpecifier);
        bool isEmpty() { return classMembers.empty(); }
	};

//class: CClassMethod - utility class, used in different classes
class CNestedType;
class CFunctionParameters;
class CClassMethod
	{
	public:
		CClassMethod();
		CClassMethod(CNestedType *name, CFunctionParameters *parameters);
		virtual ~CClassMethod();

		CNestedType *name;
		CFunctionParameters *parameters;
	};

/************************************
*
* classes derived from CClassMember
*
*************************************/


//class: CClassMemberFriend
class CNestedType;
class CClassTemplate;
class CClassMemberFriend : public CClassMember
	{
	public:
		CClassMemberFriend();
		virtual ~CClassMemberFriend();

		bool bClass;
		CNestedType *name;
		CClassTemplate *classTemplate;
	};

//class: CClassMemberNestedClass
class CClass;
class CClassMemberNestedClass : public CClassMember
	{
	public:
		CClassMemberNestedClass(CClass *nestedClass);
		virtual ~CClassMemberNestedClass();

		CClass *nestedClass;
	};

//class: CClassMemberMethod
class CTemplateSpec;
class CType;
class CClassMemberMethod : public CClassMember
	{
	public:
		CClassMemberMethod();
		virtual ~CClassMemberMethod();

		bool bVirtual;
		bool bFriend;
		bool bPureVirtual;
		bool bConstFunction;
		bool bInline;
        bool bStatic;
		CTemplateSpec *templateSpec;
		CType *returnType;
		CClassMethod *method;
	};

//class: CClassMemberData
class CStatementEnum;
class CType;
class CArrayType;
class CClassMemberData : public CClassMember
	{
	public:
		CClassMemberData();
		virtual ~CClassMemberData();

		bool bTypedef;
		bool bTypename;
		CStatementEnum *enumStatement;
		CType *type;
		std::wstring memberSimple;
		CArrayType *memberArray;
		CClassMemberMethod *memberMethod;
	};

//class: CClassMemberStatic
class CType;
class CArrayType;
class CClassMemberStatic : public CClassMember
	{
	public:
		struct StaticMember
			{
			std::wstring simpleMember;
			CArrayType *arrayMember;
			CClassMethod *methodMember;

			StaticMember();
			~StaticMember();
			};

		CClassMemberStatic();
		virtual ~CClassMemberStatic();

		CType *type;
		StaticMember *member;
	};

//class: CClassMemberConstructor
class CFunctionParameters;
class CFunctionCall;
class CClassMemberConstructor : public CClassMember
	{
	public:
		CClassMemberConstructor();
		virtual ~CClassMemberConstructor();

        	bool bCopyConstructor;
		bool bexplicitSpecifier;
		std::wstring name;
		CFunctionParameters *parameters;
		std::vector<CFunctionCall*> baseClassConstructors;
	};

//class: CClassMemberDestructor
class CFunctionParameters;
class CClassMemberDestructor : public CClassMember
	{
	public:
		CClassMemberDestructor();
		virtual ~CClassMemberDestructor();

		bool bVirtual;
		std::wstring name;
		CFunctionParameters *parameters;
	};

}

#endif
