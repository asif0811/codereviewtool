#ifndef _CREXPRUNARY
#define _CREXPRUNARY

#include "crexpressionbase.h"

namespace CodeReview
{

class CUnaryOperator;

//class: CExpressionUnaryPre

class CExpression1;
class CExpressionUnaryPre : public CExpressionBase

        {
        public:
                CUnaryOperator *unaryOp;
                CExpression1 *expression;
                bool bnegative;
                bool bnot;

                CExpressionUnaryPre();
                virtual ~CExpressionUnaryPre();
        };


//class: CExpressionUnaryPost

class CExpression2; 
class CExpressionUnaryPost : public CExpressionBase

        {
        public:
                CUnaryOperator *unaryOp;
                CExpression2 *expression;

                CExpressionUnaryPost();
                virtual ~CExpressionUnaryPost();
        };
}

#endif

