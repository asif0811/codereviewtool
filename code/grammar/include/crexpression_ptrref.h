#ifndef _CREXPRPTRREF
#define _CREXPRPTRREF

#include "crexpressionbase.h"

namespace CodeReview
{

class CExpression1;
class CExpressionPtrRef : public CExpressionBase

        {
        public:
                CExpression1 *expression;
                bool bpointer;
                bool breference;

                CExpressionPtrRef();
                virtual ~CExpressionPtrRef();
        };

}

#endif
