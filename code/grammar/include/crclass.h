#ifndef _CRCLASS
#define _CRCLASS

#include <string>
#include <vector>
#include <crfile.h>

namespace CodeReview
{

//class: CClass
class CTemplateSpec;
class CClassInheritanceElement;
class CClassMembers;
class CClass : public CCodeUnit
        {
        public:
            enum TYPE { CRCLASS, CRSTRUCT };
            CClass();
            CClass(TYPE type, std::wstring name);
        	virtual ~CClass();
        	virtual std::wstring getType();
            virtual std::wstring getInfo();
            virtual void getChildCodeUnits(std::list<CCodeUnit*>& childCodeUnits);
		
            TYPE type;
            std::wstring name;
            CTemplateSpec *templateSpec;
            std::wstring exportKeyword;
            std::vector<CClassInheritanceElement*> classinheritances;
            CClassMembers *classMembers;	
        };

    
//struct: AccessSpecifier
struct AccessSpecifier
    {
        enum ACCESS_SPECIFIER { SPECIFIER_PUBLIC, SPECIFIER_PRIVATE, SPECIFIER_PROTECTED };
        ACCESS_SPECIFIER specifier;
        
        AccessSpecifier() {specifier = SPECIFIER_PRIVATE; }
    };
    
    
//class: CClassInheritanceElement
class CNestedType;
class CClassTemplate; 
class CClassInheritanceElement
	{
	public:
		CClassInheritanceElement();
		virtual ~CClassInheritanceElement();

		bool bVirtual;
		AccessSpecifier accessSpecifier;
		CNestedType *name;
		CClassTemplate *classTemplate;
	};

}

#endif
