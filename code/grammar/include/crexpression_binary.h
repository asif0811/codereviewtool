#ifndef _CREXPRBINARY
#define _CREXPRBINARY

#include "crexpressionbase.h"

namespace CodeReview
{

class CExpression;
class CExpression2;
class COperator;
class CExpression1;

class CExpressionBinary : public CExpressionBase
    {
        public:
        //class: CExpressionBinaryLeft
		class CExpressionBinaryLeft
        {
            public:
            CExpression *expression;
            CExpression2 *expression2;
            
            CExpressionBinaryLeft();
            virtual ~CExpressionBinaryLeft();
        };
        
		CExpressionBinaryLeft *lexpression;
		COperator *binaryOp;
        CExpression1 *rexpression;
        
        CExpressionBinary();
        virtual ~CExpressionBinary();
    };

}

#endif