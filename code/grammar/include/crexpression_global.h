#ifndef _CREXPRGLOBAL
#define _CREXPRGLOBAL

#include "crexpressionbase.h"

namespace CodeReview
{

class CExpression2;
class CExpressionGlobal : public CExpressionBase
        {
        public:
                CExpression2 *expression;

                CExpressionGlobal();
                virtual ~CExpressionGlobal();
        };

}

#endif

