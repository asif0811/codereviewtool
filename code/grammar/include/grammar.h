#ifndef _CRGRAMMAR
#define _CRGRAMMAR

#include <string>
#include "crfile.h"

namespace CodeReview
{

CFile* parseCode(std::string& fileName, std::wstring& errorString);

}

#endif
