#ifndef _CREXPRCOND
#define _CREXPRCOND

#include "crexpressionbase.h"

namespace CodeReview
{

class CExpression;
class CExpressionCond : public CExpressionBase
        {
        public:
                CExpression *passExpression;
                CExpression *failExpression;

                CExpressionCond();
                virtual ~CExpressionCond();
        };

}

#endif
