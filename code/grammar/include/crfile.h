//#pragma once

#ifndef _CRFILE
#define _CRFILE

#include <list>
#include <string>

namespace CodeReview
{
class CCodeUnit;

//File class
class CFile
	{
	public:
		CFile();
		virtual ~CFile();
        void addCodeUnit(CCodeUnit *codeUnit);

		std::list<CCodeUnit*> codeUnits;
	};

//CCodeUnit class
class CCodeUnit
	{
        int lineNumber;
	public:
		CCodeUnit();
		virtual ~CCodeUnit();
        
        virtual std::wstring getType();
        virtual std::wstring getInfo();
        virtual int getLine();
        virtual void setLine(int lineNumber);
        
        virtual void getChildCodeUnits(std::list<CCodeUnit*>& childCodeUnits);
	};
}

#endif
	
