#ifndef _CREXPRBASE
#define _CREXPRBASE

namespace CodeReview
{
//All expression classes should inherit from CExpressionBase
class CExpressionBase
	{
	public:
		CExpressionBase() {};
		virtual ~CExpressionBase() {};
	};

}

#endif
