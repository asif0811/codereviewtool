#ifndef _CREXPRMEMORY
#define _CREXPRMEMORY

#include "crexpressionbase.h"

namespace CodeReview
{

class CMemoryOperator;
class CExpression1;
class CExpressionMemory : public CExpressionBase
        {
        public:
                CMemoryOperator *memOp;
                CExpression1 *expression;

                CExpressionMemory();
                virtual ~CExpressionMemory();
        };

}

#endif
