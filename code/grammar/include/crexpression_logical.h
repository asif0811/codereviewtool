#ifndef _CREXPRLOGICAL
#define _CREXPRLOGICAL

#include "crexpressionbase.h"

namespace CodeReview
{

class CLogicalOperator;
class CExpression1;
class CExpressionLogical : public CExpressionBase
        {
        public:
                CLogicalOperator *logOp;
                CExpression1 *rexpression;

                CExpressionLogical();
                virtual ~CExpressionLogical();
        };

}

#endif
