#ifndef _CRTYPE
#define _CRTYPE

#include <string>

namespace CodeReview
{

//class: CNestedType
class CNestedType
	{
	public:
		enum SPECIFIER {CRNONE, CRCONST, CRUNSIGNED, CRCONSTUNSIGNED};
		
		SPECIFIER specifier; 
		std::wstring namespace1;
		std::wstring namespace2;
		std::wstring namespace3;

		CNestedType();
		virtual ~CNestedType();
	};

//class: CType

class CClassTemplate;
class CType
	{
	public:
		CNestedType *nestedType;
		CClassTemplate *classTemplate;

		CType();
		virtual ~CType();
	};

//class: CIdentifier
class CIdentifier
	{
	public:
        enum PTROP { CRNONE, CRREF, CRPTR};
        
		CType *type;
		PTROP ptrOperator;

		CIdentifier();
		virtual ~CIdentifier();
	};

//class: CExportType
class CExportType
	{
	public:
		std::wstring keyword;
		CNestedType *nestedType;
		
		CExportType();
		virtual ~CExportType();

	};

//class: CArrayType

class CExpression;
class CArrayType
	{
	public:
		CNestedType *nestedType;
		CExpression *expression;		

		CArrayType();
		virtual ~CArrayType();
	};

//class: CArrayInitializer
class CFunctionArguments;
class CArrayInitializer
	{
	public:
		CFunctionArguments *arguments;

		CArrayInitializer();
		virtual ~CArrayInitializer();
	};

}

#endif
