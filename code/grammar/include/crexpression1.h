#ifndef _CREXPR1
#define _CREXPR1

#include "crexpressionbase.h"

namespace CodeReview
{

class CExpression;
class CExpression2;
class COperator;

class CExpression1 : public CExpressionBase
	{
	public:
		CExpressionBase *expression;
		
        CExpression1();
		CExpression1(CExpressionBase *expression);
		virtual ~CExpression1();
        
        void addBinaryExpression(CExpression *left_expression1, CExpression2 *left_expression2, COperator *binary_operator, CExpression1 *right_expression);
	};
}

#endif
