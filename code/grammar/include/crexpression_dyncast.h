#ifndef _CREXPRDYNCAST
#define _CREXPRDYNCAST

#include "crexpressionbase.h"

namespace CodeReview
{

class CClassTemplate;
class CExpression1;
class CExpressionDynCast : public CExpressionBase
        {
        public:
                CClassTemplate *classTemplate;
                CExpression1 *expression;

                CExpressionDynCast();
                virtual ~CExpressionDynCast();
        };

}

#endif
