#ifndef _CRNAMESPACE
#define _CRNAMESPACE

#include <string>
#include <list>

#include "crfile.h"

namespace CodeReview
{
    
    //class: CNamespace
    class CNamespace : public CCodeUnit
	{
	public:
		CNamespace();
        CNamespace(std::wstring name);
		virtual ~CNamespace();
        
        void addCodeUnit(CCodeUnit *codeUnit);
        virtual std::wstring getType();
        virtual std::wstring getInfo();
        virtual void getChildCodeUnits(std::list<CCodeUnit*>& childCodeUnits);
        
		std::wstring name;
		std::list<CCodeUnit*> codeUnits;
	};
    
}

#endif
