#ifndef _CREXPRCAST
#define _CREXPRCAST

#include "crexpressionbase.h"

namespace CodeReview
{

class CType;
class CExpression1;

class CExpressionCast : public CExpressionBase
	{
	public:
		CType *type;
		CExpression1 *expression;
	
		CExpressionCast();
		virtual ~CExpressionCast();
	};

}

#endif

