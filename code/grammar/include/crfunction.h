#ifndef _CRFUNCTION
#define _CRFUNCTION

#include <string>
#include <vector>

#include "crfile.h"


namespace CodeReview
{

//class: CFunction
class CFunctionSignature;
class CStatementBlock;
class CFunction : public CCodeUnit
	{
	public:
		CFunction();
		virtual ~CFunction();
        virtual std::wstring getType();
        virtual std::wstring getInfo();
        virtual void getChildCodeUnits(std::list<CCodeUnit*>& childCodeUnits);

		std::wstring comment;
		CFunctionSignature *signature;
		CStatementBlock *statementBlock;
	};

//class: CFunctionSignature
class CNestedType;
class CType;
class CFunctionParameters;
class CExportType;
class CTemplateSpec;
class CFunctionSignature
	{
	public:
		CFunctionSignature();
		virtual ~CFunctionSignature();

		CNestedType *name;
		CType *returnType;
		CFunctionParameters *parameters;
		CExportType *exportType;
		CTemplateSpec *templateSpec;

		bool bjniCall;
		bool binline;
		bool bconstFunction;
		bool bvirtual;
		bool bclassDestructor;	
	};

//class: CFunctionParameter
class CIdentifier;
class CExpression;
class CFunctionParameter
	{
	public:
		CFunctionParameter();
		virtual ~CFunctionParameter();
		
		CType *type;
		CIdentifier *name;
		CExpression *defaultValue;
	};


//class: CFunctionParameters
class CFunctionParameters
	{
        std::vector<CFunctionParameter*> parameters;
        
	public:
		CFunctionParameters();
		virtual ~CFunctionParameters();
		
        void addParameter(CFunctionParameter *parameter);
        std::vector<CFunctionParameter*>& getParameters();
        
        void getParametersCount(int &parametersCount, int &defaultParametersCount);
	};


//class: CFunctionArguments - used in CFunctionCall
class CExpression;
class CFunctionArguments
	{
        std::vector<CExpression*> arguments;
	public:
		CFunctionArguments();
		virtual ~CFunctionArguments();

        void addArgument(CExpression *argument);
        std::vector<CExpression*>& getArguments();
	};

//class: CFunctionCall
class CNestedType;
class CFunctionCall
	{
	public:
		CFunctionCall();
		virtual ~CFunctionCall();

		CNestedType *nestedType;
		CFunctionArguments *arguments;
	};

}

#endif
