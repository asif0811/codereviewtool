#ifndef _CREXPR
#define _CREXPR

#include <vector>
#include "crexpressionbase.h"

namespace CodeReview
{

//class: CExpression

class CExpression1;
class CExpression : public CExpressionBase
	{
	public:
		CExpression1 *lexpression;
		std::vector<CExpressionBase*> rexpressions;

		CExpression();
		virtual ~CExpression();
        void addRExpression(CExpressionBase *expression);
	};

//class: CExpressionLeft
//used in CStatementLR class for representing left expression

class CType;
class CExpression2;
class CExpressionLeft : public CExpressionBase
	{
	public:
		CType *type;
		CExpression2 *expression2;
		bool bStatic;
		CExpression *expression;

		CExpressionLeft();
		virtual ~CExpressionLeft();
	};

}

#endif
