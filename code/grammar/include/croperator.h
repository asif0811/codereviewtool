#ifndef _CROPERATOR
#define _CROPERATOR

namespace CodeReview
{

//Base class: COperator
class COperator
	{
	public:
		COperator(){};
		virtual ~COperator(){};
	};

//class: CUnaryOperator
class CUnaryOperator : public COperator
	{
	public:
		enum operators {INCREMENT/* ++ */, DECREMENT/* -- */};
		CUnaryOperator() {};
		virtual ~CUnaryOperator() {};
	};

//class: CClassMemberAccessOperator
class CClassMemberAccessOperator : public COperator
        {
        public:
                enum operators {DOT, POINTER};
                CClassMemberAccessOperator() {};
                virtual ~CClassMemberAccessOperator() {};
        };

//class: CShiftOperator
class CShiftOperator : public COperator
        {
        public:
                enum operators {LEFT_SHIFT, RIGHT_SHIFT};
                CShiftOperator() {};
                virtual ~CShiftOperator() {};
        };

//class: CCondOperator
class CCondOperator : public COperator
        {
        public:
                enum operators {EQ, NOTEQ, LT, LTEQ, GT, GTEQ};
                CCondOperator() {};
                virtual ~CCondOperator() {};
        };

//class: CMathOperator
class CMathOperator : public COperator
        {
        public:
                enum operators {ADD, SUBSTRACT, MULTIPLY, DIVIDE, MODULOS};
                CMathOperator() {};
                virtual ~CMathOperator() {};
        };

//class: CMemoryOperator
class CMemoryOperator : public COperator
        {
        public:
                enum operators {NEW, DELETE, DELETE_ARRAY};
                CMemoryOperator() {};
                virtual ~CMemoryOperator() {};
        };

//class: CLogicalOperator
class CLogicalOperator : public COperator
        {
        public:
                enum operators {AND, OR};
                CLogicalOperator() {};
                virtual ~CLogicalOperator() {};
        };
}

#endif

