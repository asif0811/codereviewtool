#ifndef _CRSTATEMENT
#define _CRSTATEMENT

#include <string>
#include <vector>

namespace CodeReview
{

//abstract class: CStatement

class CStatement
	{
	public:
		CStatement() {};
		virtual ~CStatement() {};
	};

//class: CStatementBlock

class CStatementBlock
	{
	public:
		CStatementBlock();
		CStatementBlock(CStatement *statement);
		virtual ~CStatementBlock();

        void addStatement(CStatement *statement);
		std::vector<CStatement*> statements;
	};


//class: CStatementLR

class CExpressionLeft;
class CExpression;
class CArrayInitializer;	
class CStatementLR : public CStatement
	{
	public:
		enum LROperator {EQUAL, PLUSEQUAL, MINUSEQUAL};		

		CStatementLR();
		virtual ~CStatementLR();

		CExpressionLeft *lexpression;
		LROperator lrOperator;
		CExpression *rexpression;
		CArrayInitializer *arrayInitializer;
		std::vector<CStatementLR*> siblingLRStatements;
	};

//class: CStatementCondition

class CExpression;
class CStatementCondition : public CStatement
	{
	public:
		CStatementCondition();
		virtual ~CStatementCondition();

		CStatementLR *condition_lr;
		CExpression *condition_exp;
		CStatementLR *statement_pass;
		CStatementLR *statement_fail;
	};



//class: CStatementEnum

class CIdentifier;
class CStatementEnum : public CStatement
	{
	public:
		struct EnumItem
			{
			EnumItem();
			~EnumItem();

			CIdentifier *name;
			CIdentifier *value;
			};

		CStatementEnum();
		virtual ~CStatementEnum();
		
		std::wstring name;
		std::vector<EnumItem*> enumItems;                
	};

//class: CStatementReturn

class CExpression;
class CStatementReturn : public CStatement
	{
	public:
		CStatementReturn();
		virtual ~CStatementReturn();

		CExpression *expression;
	};

//class: CStatementContinue

class CStatementContinue : public CStatement
	{
	public:
		CStatementContinue(){};
		virtual ~CStatementContinue() {};
	};

//class: CStatementBreak

class CStatementBreak : public CStatement
    {
    public:
        CStatementBreak(){};
        virtual ~CStatementBreak() {};
    };

//class: CStatementIf

class CStatementIf : public CStatement
	{
	public:
		CStatementIf();
		virtual ~CStatementIf();

		CStatementLR *condition;
		CStatementBlock *actionBlock;
	};

//class: CStatementElse

class CStatementElse : public CStatement
	{
	public:
		CStatementElse();
		virtual ~CStatementElse();

		CStatementBlock *statementBlock;
	};

//class: CStatementFor

class CExpression;
class CStatementFor : public CStatement
	{
	public:
		CStatementFor();
		virtual ~CStatementFor();

		CStatementLR *startStatement;
		CExpression *endExpression;
		CExpression *loopExpression;
		CStatementBlock *statementBlock;
	};	

//class: CStatementWhile

class CExpression;
class CStatementWhile : public CStatement
	{
	public:
		CStatementWhile();
		virtual ~CStatementWhile();

		CExpression *condition;
		CStatementBlock *statementBlock;
	};

//class: CStatementTryCatch

class CStatementTryCatch : public CStatement
	{
	public:
		CStatementTryCatch();
		virtual ~CStatementTryCatch();

		CStatementBlock *tryBlock;
		CStatementBlock *catchBlock;
	};

//class: CStatementSwitchCase

class CExpression;
class CCaseStatement
	{
	public:
		CCaseStatement();
		~CCaseStatement();

		CExpression *condition;
		CStatementBlock *conditionActionBlock;
		CStatementBlock *defaultActionBlock;
	};

class CStatementSwitchCase : public CStatement
	{
	public:
		CStatementSwitchCase();
		virtual ~CStatementSwitchCase();

		CExpression *switchExpression;
		std::vector<CCaseStatement*> caseStatements;
	};

}

#endif
