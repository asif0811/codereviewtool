#ifndef _CREXPR2
#define _CREXPR2

#include "crexpressionbase.h"

namespace CodeReview
{

class CIdentifier;
class CFunctionCall;
class CArrayType;
class CExpression2 : public CExpressionBase
	{
	public:
		bool bnumberZero;
		bool bnumber;
		bool bconstString;

		CIdentifier *identifier;
		CFunctionCall *functionCall;
		CArrayType *arrayType;

		CExpression2();
		virtual ~CExpression2();
	};
}

#endif

