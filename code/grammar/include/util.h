//#pragma once

#ifndef _CRUTIL
#define _CRUTIL

#include <string>

namespace CodeReview
{
    //forward declarations
    class CCodeUnit;
    class CFile;
    class CFunctionSignature;
    class CStatementBlock;
    
    std::wstring toString(std::string pString);
    
    CCodeUnit* addToCodeUnit(CFile *crFile, CFunctionSignature *crFuncSignature, CStatementBlock *crStatementBlock);
}

#endif
