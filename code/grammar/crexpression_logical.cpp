#include <cstddef>

#include "crexpression_logical.h"
#include "croperator.h"
#include "crexpression1.h"

namespace CodeReview
{

CExpressionLogical::CExpressionLogical()
	{
	logOp = NULL;
	rexpression = NULL;
	}

CExpressionLogical::~CExpressionLogical()
	{
	if (logOp != NULL)
		delete logOp;
	
	if (rexpression != NULL)
		delete rexpression;
	}

}
