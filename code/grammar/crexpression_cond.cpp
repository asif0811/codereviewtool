#include "crexpression_cond.h"
#include "crexpression.h"

namespace CodeReview
{

CExpressionCond::CExpressionCond()
	{
	passExpression = NULL;
	failExpression = NULL;
	}

CExpressionCond::~CExpressionCond()
	{
	if (passExpression != NULL)
		delete passExpression;

	if (failExpression != NULL)
		delete failExpression;
	}

}
