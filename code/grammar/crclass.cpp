#include "crclass.h"
#include "crtemplate.h"
#include "crclassmembers.h"
#include "crtype.h"

namespace CodeReview
{

CClass::CClass()
	{
	type = CRCLASS;
	templateSpec = NULL;
	classMembers = NULL;
	}

CClass::CClass(TYPE type, std::wstring name)
	{
    //default values
    this->type = CRCLASS;
	this->templateSpec = NULL;
	this->classMembers = NULL;

	this->type = type;
    this->name = name;
	}

CClass::~CClass()
	{
	if (templateSpec != NULL)
		delete templateSpec;

    std::vector<CClassInheritanceElement*>::iterator it = classinheritances.begin();
    while(it != classinheritances.end())
    {
        CClassInheritanceElement *inheritanceElement = *it;
        ++it;
        if (inheritanceElement != NULL)
            delete inheritanceElement;
    }
	
	if (classMembers != NULL)
		delete classMembers;
	}

std::wstring CClass::getType()
    {
        return std::wstring(L"Class");
    }
    
std::wstring CClass::getInfo()
    {
        std::wstring info = L"class name: ";
        if (type == CRSTRUCT)
            info = L"struct name: ";
        info = info + name;
        return info;
    }

void CClass::getChildCodeUnits(std::list<CCodeUnit*>& childCodeUnits)
    {
        if (this->classMembers == NULL)
            return;
        
        for(std::vector<CClassMember*>::iterator it = this->classMembers->classMembers.begin(); it != this->classMembers->classMembers.end(); ++it)
            {
                CClassMemberNestedClass* crNestedClass = dynamic_cast<CClassMemberNestedClass*>(*it);
                if (crNestedClass && crNestedClass->nestedClass)
                {
                    childCodeUnits.push_back(crNestedClass->nestedClass);
                    (crNestedClass->nestedClass)->getChildCodeUnits(childCodeUnits);
                }
            }
    }

CClassInheritanceElement::CClassInheritanceElement()
	{
	bVirtual = false;
    accessSpecifier.specifier = AccessSpecifier::SPECIFIER_PRIVATE;
	name = NULL;
	classTemplate = NULL;
	}

CClassInheritanceElement::~CClassInheritanceElement()
	{
	if (name != NULL)
		delete name;
	
	if (classTemplate != NULL)
		delete classTemplate;
	}
}
