#include "crnamespace.h"

namespace CodeReview
{
    
    CNamespace::CNamespace()
	{}
    
    CNamespace::CNamespace(std::wstring name)
    {
        this->name = name;
    }
    
    CNamespace::~CNamespace()
	{
        std::list<CCodeUnit*>::iterator it = codeUnits.begin();
        while(it != codeUnits.end())
        {
            CCodeUnit *codeUnit = *it;
            ++it;
            if (codeUnit != NULL)
                delete codeUnit;
        }
	}
    
    void CNamespace::addCodeUnit(CCodeUnit *codeUnit)
    {
        if(codeUnit)
            codeUnits.push_back(codeUnit);
    }
    
    std::wstring CNamespace::getType()
    {
        return std::wstring(L"Namespace");
    }
    
    std::wstring CNamespace::getInfo()
    {
        std::wstring info = L"name: " + name;
        return info;
    }
    
    void CNamespace::getChildCodeUnits(std::list<CCodeUnit*>& childCodeUnits)
    {
        std::list<CCodeUnit*>::iterator it = codeUnits.begin();
        while(it != codeUnits.end())
        {
            CCodeUnit *codeUnit = *it;
            if (codeUnit)
            {
                childCodeUnits.push_back(codeUnit);
                codeUnit->getChildCodeUnits(childCodeUnits);
            }
            ++it;
        }
    }
    
}
