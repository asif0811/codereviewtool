#include <map>
#include <iostream>

#include <crfile.h>
#include <analyzer.h>
#include <prrRule.h>

int main()
{
    try {
        std::cout << std::endl << "UnitTest: Code review unit test...";
        
        //create a file containing the input code
        std::string codeFileName = "../unittest/files/code";
        std::cout << std::endl << "UnitTest: Input file - " << codeFileName;
        
        //parse source code
        CodeReview::CFile* cFile = CodeReview::parseSourceCode(codeFileName);
        if (cFile == NULL)
        {
            std::cout << std::endl << "UnitTest: Parsing source code failed !";
            return 0;
        }
        std::cout << std::endl << "UnitTest: Successfully parsed source code file";
        
        //generate rule sets
        std::string ruleFileName = "../unittest/files/rules";
        std::cout << std::endl << "UnitTest: Rule file - " << ruleFileName;
        std::map<std::wstring, PRR::RuleSet*> ruleSets;
        CodeReview::generateRuleSet(ruleFileName, cFile, ruleSets);
        if (ruleSets.empty())
    	{
            std::cout << std::endl << L"UnitTest: Failed while generating rule sets !";
       	    return 0;
    	}
        std::cout << std::endl << "UnitTest: Successfully generated rule sets";
        
        //review code
        std::string resultsFile = "../unittest/files/result";
        std::cout << std::endl << "UnitTest: Results file - " << resultsFile;
        CodeReview::evaluateRuleSet(cFile,ruleSets,resultsFile);
        
        std::map<std::wstring, PRR::RuleSet*>::iterator it = ruleSets.begin();
        while(it != ruleSets.end())
        {
            PRR::RuleSet *pRuleSet = it->second;
            ++it;
            if (pRuleSet != NULL)
                delete pRuleSet;
        }
        
        if (cFile)
            delete cFile;
        
        std::cout << std::endl << "UnitTest: Successfully finished.....congrats !!!!";
        
    } catch (...) {
        std::cout << std::endl << "UnitTest: Exception while reviewing code.";
    }

    return 0;
}
