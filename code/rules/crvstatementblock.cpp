
#include <crstatement.h>
#include <crexpression.h>
#include <crexpression1.h>
#include <crexpression2.h>
#include <crfunction.h>
#include "crvstatementblock.h"

namespace CodeReview
{

CRVStatementBlock::CRVStatementBlock(CStatementBlock *statementBlock)
	{
	m_statementBlock = statementBlock;
	}

CRVStatementBlock::~CRVStatementBlock()
	{
	//m_statementBlock = NULL;
	}

void CRVStatementBlock::setStatementBlock(CStatementBlock *statementBlock)
	{
	m_statementBlock = statementBlock;
	}


void CRVStatementBlock::getfunctionCalls(std::vector<CFunctionCall*>& functionCalls)
	{
	if (m_statementBlock == NULL)
		return;

	for(std::vector<CStatement*>::iterator it = m_statementBlock->statements.begin(); it != m_statementBlock->statements.end(); ++it)
		{
		CStatement *statement = *it;
		CStatementLR *statementLR = dynamic_cast<CStatementLR*>(statement);
		
		//get function calls from left side of an expression
		if (statementLR != NULL && statementLR->lexpression != NULL && statementLR->lexpression->expression != NULL)
			{
			CExpression *expression = statementLR->lexpression->expression;
			if (expression->lexpression != NULL && expression->lexpression->expression != NULL)
				{
				CExpression2 *expression2 = dynamic_cast<CExpression2*>(expression->lexpression->expression);
				if (expression2 != NULL && expression2->functionCall != NULL)
					functionCalls.push_back(expression2->functionCall);	
				}
			}
		
		//get function calls from right side of an expression
		if (statementLR != NULL && statementLR->rexpression != NULL)
			{
			CExpression *expression = statementLR->rexpression;
                        if (expression->lexpression != NULL && expression->lexpression->expression != NULL)
                                {
                                CExpression2 *expression2 = dynamic_cast<CExpression2*>(expression->lexpression->expression);
                                if (expression2 != NULL && expression2->functionCall != NULL)
                                        functionCalls.push_back(expression2->functionCall);
                                }
			}		
		}
	}

}
