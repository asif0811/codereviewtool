
#include <vector>
#include <cstdio>
#include <json/json.h>

#include <crfile.h>
#include <crclass.h>
#include <crclassmembers.h>
#include <crfunction.h>
#include <crtype.h>

#include "crvruleattribute.h"
#include "crvstatementblock.h"
#include "crvutils.h"
#include "crvclass.h"

#include <iostream>
#include <fstream>
#include <stdio.h>

namespace CodeReview
{
/*
    void CRVClass::PrintJSONValue( const Json::Value &val )
    {
        if( val.isString() ) {
            printf( "%s", val.asString().c_str() );
        } else if( val.isBool() ) {
            printf( "bool(%d)", val.asBool() );
        } else if( val.isInt() ) {
        } else if( val.isUInt() ) {
            printf( "uint(%u)", val.asUInt() );
        } else if( val.isDouble() ) {
            printf( "double(%f)", val.asDouble() );
        }
        else
        {
            printf( "unknown type=[%d]", val.type() );
        }
    }

    
    bool CRVClass::PrintJSONTree(const Json::Value& root, unsigned short depth)
    {
        depth += 1;
        printf("");
        //printf( " {type=[%d], size=%d}", root.type(), root.size() );
        //printf( " size=%d", root.type(), root.size() );
        
        if( root.size() > 0 ) {
            printf("\n");
            for( Json::ValueConstIterator itr = root.begin() ; itr != root.end() ; itr++ ) {
                // Print depth.
                for( int tab = 0 ; tab < depth; tab++) {
                    printf("-");
                }
                //printf(" subvalue(");
                //printf(" (");
                PrintJSONValue(itr.key());
                //printf(") -");
                PrintJSONTree( *itr, depth);
            }
            return true;
        } else {
            printf(" ");
            PrintJSONValue(root);
            printf( "\n" ); 
        }
        return true;
    }
*/

CRVClass::CRVClass(CCodeUnit *pCodeUnit, bool &initialized)
	{
        this->initialized = false;
        m_class = static_cast<CClass*>(pCodeUnit);
        initialized = initialize();
	}

CRVClass::~CRVClass()
	{
        std::map<std::wstring, CRVAttribute*>::iterator it = m_attributes.begin();
        while(it != m_attributes.end())
        {
            CRVAttribute *crvAttribute = it->second;
            ++it;
            if (crvAttribute != NULL)
                delete crvAttribute;
        }
    }

bool CRVClass::initialize()
	{
        
        Json::Value root;
        Json::Reader reader;
        
        std::ifstream file("code_attributes.json");
        if (!reader.parse(file, root, true))
        {
            std::cout  << "Failed to parse configuration\n" << reader.getFormattedErrorMessages();
        }
        else {
            //PrintJSONTree(root);
        }

        if (m_class == NULL)
        {
            //return an error indicating that rule variable cannot be set
            logError(L"Rule variable for CClass cannot be set. NULL class object");
            return initialized;
		}
        
        //handle if an empty class(class without any data members and with no inheritance) object is passed
        if (m_class->classinheritances.empty() && (m_class->classMembers == NULL || m_class->classMembers->isEmpty()))
        {
            logError(L"Rule variable for CClass cannot be set. Empty class object");
            return initialized;
        }

        //available attributes
        m_attributes[L"Type"] = new CRVSingleAttribute(L"Type");
        m_attributes[L"Name"] = new CRVMapAttribute(L"Name");
        m_attributes[L"InheritanceType"] = new CRVSingleAttribute(L"InheritanceType");
        m_attributes[L"Destructor"] = new CRVMapAttribute(L"Destructor");
        m_attributes[L"Constructor"] = new CRVMapAttribute(L"Constructor");
        m_attributes[L"Method"] = new CRVMapAttribute(L"Method");
        m_attributes[L"DataMember"] = new CRVMapAttribute(L"DataMember");
        m_attributes[L"VirtualMethod"] = new CRVMapAttribute(L"VirtualMethod");

       	if (m_class->type == CClass::CRCLASS)
            m_attributes[L"Type"]->setValue(L"Class", L"true");
        if (m_class->type == CClass::CRSTRUCT)
            m_attributes[L"Type"]->setValue(L"Struct", L"true");
        if (m_class->templateSpec)
            m_attributes[L"Type"]->setValue(L"TemplateClass", L"true");
        if (!m_class->exportKeyword.empty())
            m_attributes[L"Type"]->setValue(L"ExportedClass", L"true");
        
        if (!startsWithUppercase(m_class->name))
            m_attributes[L"Name"]->setValue(L"NameStartsWithUppercase", L"false");
       
        if(!m_class->classinheritances.empty())
		{
            m_attributes[L"Type"]->setValue(L"DerivedClass", L"true");
            std::vector<CClassInheritanceElement*>::iterator it = m_class->classinheritances.begin();
            AccessSpecifier accessSpecifier;
            accessSpecifier.specifier = (*it)->accessSpecifier.specifier;
            switch(accessSpecifier.specifier)
			{
            case AccessSpecifier::SPECIFIER_PUBLIC:
                    m_attributes[L"InheritanceType"]->setValue(L"Public", L"true");
                    break;
			case AccessSpecifier::SPECIFIER_PRIVATE:
                    m_attributes[L"InheritanceType"]->setValue(L"Private", L"true");
                    break;
			case AccessSpecifier::SPECIFIER_PROTECTED:
                    m_attributes[L"InheritanceType"]->setValue(L"Protected", L"true");
                    break;
			default:
                    //default is private
                    m_attributes[L"InheritanceType"]->setValue(L"Private", L"true");
                    break;
			}
            
            for(it = m_class->classinheritances.begin()+1 ; it != m_class->classinheritances.end(); ++it)
			{
                if ((*it)->accessSpecifier.specifier != accessSpecifier.specifier)
                    m_attributes[L"InheritanceType"]->setValue(L"Mixed", L"true");
			}
		}
        
        CRVStatementBlock constructorStmntBlock(NULL);
        std::vector<CClassMemberMethod*> classMemberMethods;

        if (m_class->classMembers != NULL)
		{
            //initialization of class member specific attribute default values
            CClassMembers *classMembers = m_class->classMembers;
            for(std::vector<CClassMember*>::iterator it = classMembers->classMembers.begin(); it != classMembers->classMembers.end(); ++it)
			{
                CClassMember *classMember = *it;
                if (classMember == NULL)
                    continue;
			
                std::string member_typename = typeid(*classMember).name();
                CClassMemberMethod *classMemberMethod = dynamic_cast<CClassMemberMethod*>(classMember);
                CClassMemberData *classMemberData = dynamic_cast<CClassMemberData*>(classMember);
                CClassMemberStatic *classMemberStatic = dynamic_cast<CClassMemberStatic*>(classMember);
                CClassMemberDestructor *classDestructor = dynamic_cast<CClassMemberDestructor*>(classMember);
                CClassMemberConstructor *classConstructor = dynamic_cast<CClassMemberConstructor*>(classMember);
                if (classMemberMethod != NULL)
				{
                    classMemberMethods.push_back(classMemberMethod);
                    if (classMemberMethod->bVirtual || classMemberMethod->bPureVirtual)
                    {
                        m_attributes[L"Method"]->setValue(L"VirtualMethod", L"true");
                        CClassMethod *method = classMemberMethod->method;
                        if (method && method->parameters)
                        {
                            std::vector<CFunctionParameter*> functionParameters = method->parameters->getParameters();
                            for(std::vector<CFunctionParameter*>::iterator it = functionParameters.begin(); it != functionParameters.end(); ++it)
                            {
                                CFunctionParameter *functionParameter = *it;
                                if (functionParameter && functionParameter->defaultValue)
                                {
                                    m_attributes[L"VirtualMethod"]->setValue(L"DefaultArguments", L"true");
                                    break;
                                }
                            }
                        }
                    }
                    if (classMemberMethod->method != NULL && classMemberMethod->method->name != NULL)
                        startsWithUppercase(classMemberMethod->method->name->namespace1) ?
                        m_attributes[L"Method"]->setValue(L"NameStartsWithUppercase", L"true")
                        : m_attributes[L"Method"]->setValue(L"NameStartsWithUppercase", L"false");
				}
                if (classMemberData != NULL)
				{
                    if (classMemberData->memberMethod != NULL)
					{
                        classMemberMethods.push_back(classMemberData->memberMethod);
                        std::wstring methodname = classMemberData->memberMethod->method->name->namespace1;
                        if (methodname.compare(L"operator=") == 0)
                            m_attributes[L"Method"]->setValue(L"AssignmentOperator", L"true");
                        if (methodname.find(L"operator") != 0)
                            startsWithUppercase(methodname) ? m_attributes[L"Method"]->setValue(L"NameStartsWithUppercase", L"true")
										  : m_attributes[L"Method"]->setValue(L"NameStartsWithUppercase", L"false");
                        if ((methodname.compare(L"operator&&") == 0) || (methodname.compare(L"operator||") == 0)
                            || (methodname.compare(L"operator,") == 0))
                            m_attributes[L"Method"]->setValue(L"ValidOperatorOverloads", L"false");
					}
                    if (!(classMemberData->memberSimple.empty()))
					{
                        //by default members are private
                        if (classMemberData->accessSpecifier.specifier != AccessSpecifier::SPECIFIER_PRIVATE)
				        	m_attributes[L"DataMember"]->setValue(L"PrivateMember", L"false");
                        if (!allLowerCase(classMemberData->memberSimple))
                            m_attributes[L"DataMember"]->setValue(L"NameIsLowercase", L"false");
					}
                    if (classMemberData->memberArray != NULL)
					{
                        if (classMemberData->accessSpecifier.specifier != AccessSpecifier::SPECIFIER_PRIVATE)
                            m_attributes[L"DataMember"]->setValue(L"PrivateMember", L"false");
                        if (classMemberData->memberArray->nestedType != NULL && !allLowerCase(classMemberData->memberArray->nestedType->namespace1))
                            m_attributes[L"DataMember"]->setValue(L"NameIsLowercase", L"false");
					}
				}
                if (classMemberStatic != NULL && classMemberStatic->member != NULL
                                && (!(classMemberStatic->member->simpleMember.empty()) || classMemberStatic->member->arrayMember != NULL))
				{
                    if (classMemberStatic->accessSpecifier.specifier != AccessSpecifier::SPECIFIER_PRIVATE && classMemberStatic->type != NULL
                        && classMemberStatic->type->nestedType != NULL && classMemberStatic->type->nestedType->specifier != CNestedType::CRCONST)
                        m_attributes[L"DataMember"]->setValue(L"PrivateMember", L"false");
                    if (!allLowerCase(classMemberStatic->member->simpleMember)
                        || (classMemberStatic->member->arrayMember != NULL && !allLowerCase(classMemberStatic->member->arrayMember->nestedType->namespace1)))
                        m_attributes[L"DataMember"]->setValue(L"NameIsLowercase", L"false");
				}
                if (classDestructor != NULL && classDestructor->bVirtual == true)
                    m_attributes[L"Destructor"]->setValue(L"VirtualDestructor", L"true");
                if (classConstructor != NULL)
                {
                    if (classConstructor->bCopyConstructor)
                        m_attributes[L"Constructor"]->setValue(L"CopyConstructor", L"true");
                    if (classConstructor->parameters != NULL)
					{
                        char sizeBuffer[20] = {0};
                        sprintf(sizeBuffer, "%d", (int)classConstructor->parameters->getParameters().size());
                        std::string paramsSize(sizeBuffer);
                        std::wstring strParamsSize(paramsSize.begin(), paramsSize.end());
                        //m_attributes[L"Constructor"]->setValue(L"ParametersCount", strParamsSize);
					}
                    if (classConstructor->bexplicitSpecifier)
                        m_attributes[L"Constructor"]->setValue(L"ExplicitConstructor", L"true");
					
                    constructorStmntBlock.setStatementBlock(classConstructor->statementBlock);
                }
			}

            std::vector<CFunctionCall*> functionCalls;
            constructorStmntBlock.getfunctionCalls(functionCalls);
            for (std::vector<CFunctionCall*>::iterator it = functionCalls.begin(); it != functionCalls.end(); ++it)
			{
                CFunctionCall *functionCall = *it;
                for (std::vector<CClassMemberMethod*>::iterator it1 = classMemberMethods.begin(); it1 != classMemberMethods.end(); ++it1)
				{
                    CClassMemberMethod *classMethod = *it1;
                    if (classMethod->bVirtual == true && classMethod->method != NULL &&
                        classMethod->method->name->namespace1.compare(functionCall->nestedType->namespace1) == 0)
					{
                        int functionParametersCount = 0, classMethodParametersCount = 0, classMethodDefaultParametersCount = 0;
                        if (functionCall->arguments != NULL)
                            functionParametersCount = functionCall->arguments->getArguments().size();
                        if (classMethod->method->parameters != NULL)
                            classMethod->method->parameters->getParametersCount(classMethodParametersCount, classMethodDefaultParametersCount);
                        if ((functionParametersCount >= (classMethodParametersCount - classMethodDefaultParametersCount)) &&
                            (functionParametersCount <= classMethodParametersCount))
                            m_attributes[L"Constructor"]->setValue(L"VirtualMethodCalls", L"true");
					}
				}
			}
		}
        
        initialized = true;
        return true;
	}

void CRVClass::evaluate(std::wstring propertyname, OCL::Any& propertyvalue)
	{
	if (!initialized)
		return;

    std::wstring attributeValue = getAttributeValue(m_attributes, propertyname);
    if (attributeValue.empty())
        propertyvalue = OCL::Invalid();
        
    propertyvalue.m_value.string = attributeValue;
	}

}
