#ifndef _CRVCLASS
#define _CRVCLASS

#include <string>
//#include <json/json.h>

#include <prrRuleInterface.h>

namespace CodeReview
{
//forward declarations
class CClass;
class CCodeUnit;
class CRVAttribute;

//rule variable class for CClass
//class: CRVClass
class CRVClass : public PRR::RuleInterface
	{
	CClass* m_class;

	//Attributes for CClass
	std::map<std::wstring, CRVAttribute*> m_attributes;

	public:
		CRVClass(CCodeUnit *pCodeUnit, bool &initialized);
		virtual ~CRVClass();
		virtual void evaluate(std::wstring propertyname, OCL::Any& propertyvalue);
                //bool PrintJSONTree(const Json::Value& root, unsigned short depth = 0);
                //void PrintJSONValue( const Json::Value &val );
	
	protected:
		virtual bool initialize();
	};

}

#endif
