#ifndef _CRVFUNCTION
#define _CRVFUNCTION

#include <string>
#include <prrRuleInterface.h>

namespace CodeReview
{
//forward declarations

class CCodeUnit;
class CFunction;
class CRVAttribute;

//rule variable class for CFunction
//class: CRVFunction
class CRVFunction : public PRR::RuleInterface
	{
	CFunction* m_function;

	//Attributes for CFunction
	std::map<std::wstring, CRVAttribute*> m_attributes;

	public:
		CRVFunction(CCodeUnit *pCodeUnit, bool &initialized);
		virtual ~CRVFunction();
		virtual void evaluate(std::wstring propertyname, OCL::Any& propertyvalue);
	
	protected:
		virtual bool initialize();
	};
}

#endif

