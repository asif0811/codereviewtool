#ifndef _CRVUTILS
#define _CRVUTILS

#include <string>

namespace CodeReview
{
//forward declarations
class CRVAttribute;

std::wstring getAttributeValue(std::map<std::wstring, CRVAttribute*>& attributes, const std::wstring& attributeName);

bool startsWithUppercase(std::wstring& name);

bool allLowerCase(std::wstring& name);
}

#endif
