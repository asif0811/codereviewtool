#ifndef _CRVSTMNTBLOCK
#define _CRVSTMNTBLOCK

#include <vector>

namespace CodeReview
{
//forward declarations
class CStatementBlock;
class CFunctionCall;

class CRVStatementBlock
	{
	CStatementBlock *m_statementBlock;

	public:
		CRVStatementBlock(CStatementBlock *statementBlock);
		virtual ~CRVStatementBlock();

		void setStatementBlock(CStatementBlock *statementBlock);
		void getfunctionCalls(std::vector<CFunctionCall*>& functionCalls);
	};

}

#endif

