#ifndef _CRVRULEATTRIBUTE
#define _CRVRULEATTRIBUTE

#include <string>
#include <map>

namespace CodeReview
{

class CRVAttribute
	{

	std::wstring m_attributeName;
	
	public:
		CRVAttribute(std::wstring attributeName); 
		virtual ~CRVAttribute();
		
		virtual void setValue(std::wstring value) {};
		virtual void setValue(std::wstring key, std::wstring value) {};
		virtual std::wstring getValue() { return L""; };
		virtual std::wstring getValue(std::wstring key) { return L""; };
	};


class CRVSingleAttribute : public CRVAttribute
        {

        //Key snd attribute value
        std::wstring m_key;
        std::wstring m_value;
        
        public:
            CRVSingleAttribute(std::wstring attributeName);
            virtual ~CRVSingleAttribute();

            virtual void setValue(std::wstring value);
            virtual void setValue(std::wstring key, std::wstring value);
            virtual std::wstring getValue();
            virtual std::wstring getValue(std::wstring key);
        };


class CRVMapAttribute : public CRVAttribute
        {

        //Attributes
        std::map<std::wstring, std::wstring> m_mapAttribute;
        
        public:
            CRVMapAttribute(std::wstring attributeName);
            virtual ~CRVMapAttribute();

            virtual void setValue(std::wstring key, std::wstring value);
            virtual std::wstring getValue();
            virtual std::wstring getValue(std::wstring key);
        };

}

#endif
