#include <string>
#include <locale>

#include "crvruleattribute.h"
#include "crvutils.h"

namespace CodeReview
{

std::wstring getAttributeValue(std::map<std::wstring, CRVAttribute*>& attributes, const std::wstring& attributeName)
        {

	if (attributes.empty())
        return L"";

	std::wstring attributeProperty, newAttributeName = attributeName;
	std::size_t pos = attributeName.find(L".");
	if (pos != std::wstring::npos)
		{
		newAttributeName = attributeName.substr(0,pos);
		attributeProperty = attributeName.substr(pos + 1);
		}

	if (attributes.find(attributeName) != attributes.end())
		return attributes[attributeName]->getValue();
    else if (attributes.find(newAttributeName) != attributes.end())
		return attributes[newAttributeName]->getValue(attributeProperty);
	else
		return L"";

        }

bool startsWithUppercase(std::wstring& name)
	{
	bool startsUpperCase = false;
	if (name.empty())
		return startsUpperCase;

	if (std::isupper(name[0]))
		startsUpperCase = true;

	return startsUpperCase;	
	}

bool allLowerCase(std::wstring& name)
	{
	bool lowerCase = true;
	std::locale loc;

	if (name.empty())
		return lowerCase;

	for (std::wstring::size_type i = 0; i < name.length(); ++i)
		{
		if (std::isalpha(name[i]) && !std::islower(name[i]))
			{
			lowerCase = false;
			break;
			}
		}

	return lowerCase;
	}
}
