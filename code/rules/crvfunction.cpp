#include <vector>
#include <fstream>

#include <json/json.h>

#include <crfunction.h>
#include <crtype.h>

#include "crvruleattribute.h"
#include "crvutils.h"
#include "crvfunction.h"

namespace CodeReview
{
    
CRVFunction::CRVFunction(CCodeUnit *pCodeUnit, bool &initialized)
	{
        this->initialized = false;
        m_function = static_cast<CFunction*>(pCodeUnit);
        initialized = initialize();
	}

CRVFunction::~CRVFunction()
	{
        std::map<std::wstring, CRVAttribute*>::iterator it = m_attributes.begin();
        while(it != m_attributes.end())
        {
            CRVAttribute *crvAttribute = it->second;
            ++it;
            if (crvAttribute != NULL)
                delete crvAttribute;
        }
    }

bool CRVFunction::initialize()
	{
        
        Json::Value root;
        Json::Reader reader;
        
        std::ifstream file("code_attributes.json");
        if (!reader.parse(file, root, true))
            std::cout  << "Failed to parse configuration\n" << reader.getFormattedErrorMessages();

        if (m_function == NULL)
        {
            //return an error indicating that rule variable cannot be set
            logError(L"Rule variable for CFunction cannot be set. NULL function object");
            return initialized;
		}
        
        //handle if an invalid function object is passed
        if (m_function->signature == NULL)
        {
            logError(L"Rule variable for CFunction cannot be set. Invalid function object");
            return initialized;
        }

        //available attributes
        CFunctionSignature *functionSignature = m_function->signature;
        m_attributes[L"Name"] = new CRVMapAttribute(L"Name");
        m_attributes[L"Argument"] = new CRVMapAttribute(L"Argument");
        
        std::wstring functionName;
        CNestedType *nestedTypeFuncName = functionSignature->name;
        if (nestedTypeFuncName)
        {
            functionName = nestedTypeFuncName->namespace3;
            if (functionName.empty())
                functionName = nestedTypeFuncName->namespace2;
            if (functionName.empty())
                functionName = nestedTypeFuncName->namespace1;
            if (!startsWithUppercase(functionName))
                m_attributes[L"Name"]->setValue(L"NameStartsWithUppercase", L"false");
        }

       
        if(functionSignature->parameters)
		{
            std::vector<CFunctionParameter*> funcParametersList = functionSignature->parameters->getParameters();
            for(std::vector<CFunctionParameter*>::iterator it = funcParametersList.begin(); it != funcParametersList.end(); ++it)
            {
                CFunctionParameter *funcParameter = *it;
                if (funcParameter && funcParameter->name && funcParameter->name->ptrOperator == CIdentifier::CRREF)
                {
                    CNestedType *nestedType = funcParameter->type->nestedType;
                    if (nestedType && nestedType->specifier != CNestedType::CRCONST)
                        m_attributes[L"Argument"]->setValue(L"ReferenceIsConst", L"false");
                }
            }
        }
        
        initialized = true;
        return true;
	}

void CRVFunction::evaluate(std::wstring propertyname, OCL::Any& propertyvalue)
	{
	if (!initialized)
		return;

    std::wstring attributeValue = getAttributeValue(m_attributes, propertyname);
    propertyvalue.m_value.string = attributeValue;
	}

}

