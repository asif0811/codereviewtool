#include "crvruleattribute.h"

namespace CodeReview
{

CRVAttribute::CRVAttribute(std::wstring attributeName)
	{
	m_attributeName = attributeName;	
	}

CRVAttribute::~CRVAttribute()
	{
	}

CRVSingleAttribute::CRVSingleAttribute(std::wstring attributeName) : CRVAttribute(attributeName)
	{
    m_key = L"";
	m_value = L"";
	}

CRVSingleAttribute::~CRVSingleAttribute()
	{
	}

void CRVSingleAttribute::setValue(std::wstring value)
    {
        // not-implemented. not required for now
    }

void CRVSingleAttribute::setValue(std::wstring key, std::wstring value)
	{
        m_key = key;
        m_value = value;
    }

std::wstring CRVSingleAttribute::getValue()
	{
        // not-implemented. not required for now
        return L"";
	}

std::wstring CRVSingleAttribute::getValue(std::wstring key)
	{
        if (key.compare(m_key) == 0)
            return m_value;
        else
            return L"false"; //re-factor this code
    }

CRVMapAttribute::CRVMapAttribute(std::wstring attributeName) : CRVAttribute(attributeName)
	{
	}

CRVMapAttribute::~CRVMapAttribute()
	{
	}

void CRVMapAttribute::setValue(std::wstring key, std::wstring value)
	{
    //key already has value. need to do an X-OR here
    if (m_mapAttribute.count(key) == 1 && (value == L"true" || value == L"false") && value.compare(m_mapAttribute[key]) != 0)
        m_mapAttribute[key] = L"false";
    else
        m_mapAttribute[key] = value;
	}
    
std::wstring CRVMapAttribute::getValue()
	{
        // not-implemented. not required for now
        return L"";
	}

std::wstring CRVMapAttribute::getValue(std::wstring key)
	{
	if (m_mapAttribute.count(key) == 1)
		return m_mapAttribute[key];
	else 
		return L"";
	}

}
