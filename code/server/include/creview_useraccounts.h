#ifndef __CREVIEWUSERACCOUNTS
#define __CREVIEWUSERACCOUNTS

#include <string>
#include <map>


/* 
 class to create / manage user accounts
*/

//forward declarations
namespace Wt {
    class WApplication;
    class WText;
    class WDialog;
    class WLineEdit;
}

class CodeReviewAccountPage : public Wt::WObject
{
public:
    CodeReviewAccountPage(Wt::WApplication *application);
    virtual ~CodeReviewAccountPage();
    void ShowPage(Wt::WText *welcomeMessage);
    void addUserSession(std::string sessionId, std::string userName);
    std::string getCurrentUserName() { return userName->text().toUTF8(); }
    std::string getCurrentUserPassword() { return password->text().toUTF8(); }
    void closeDialog() { return accountDlg->accept(); }
    void setWelcomeMessage();
    
private:
    Wt::WApplication *application;
    Wt::WDialog *accountDlg;
    Wt::WLineEdit *userName;
    Wt::WLineEdit *password;
    Wt::WText *welcomeMessage;
    
    std::map<std::string, std::string> userSessions;
    
    void CreatePage();
    void Login();
    
};

#endif

