#ifndef __CREVIEWRULES
#define __CREVIEWRULES

#include <string>
#include <map>
#include <vector>

#include <Wt/WTable>
/* 
 Persist rules in the application to disk and re-create rules from the disk in the application for a user
*/

//forward declarations
struct RuleCategory;
namespace CodeReview {
    class CodeAttributes;
}

namespace CodeReviewServer
{
    /* Persist rules in the UI to a rules file */
    
    struct RuleCategory
    {
        std::string name;
        Wt::WPushButton *button;
        Wt::WTable *rulesTable;
        CodeReview::CodeAttributes *attributes;
    };
    
    class CodeReviewRulesPersister
    {
    public:
        CodeReviewRulesPersister(std::string userName);
        virtual ~CodeReviewRulesPersister();
    
        bool persistRules(std::map<std::string, RuleCategory*>& ruleCategories);
    
    private:
        std::string userName;
    
        bool writeRulestoFile(std::ofstream& rulesFile, std::string ruleCategory,
                          Wt::WTable* rulesInTable, CodeReview::CodeAttributes *categoryAttributes);
        std::string getOperator(std::string operatorText);
        std::string getComboBoxValue(Wt::WWidget* widget);
    
    };
    

    /* Load rules in the file back to UI */
    struct RuleElement
    {
        std::string category;
        std::string attribute;
        std::string property;
        std::string property_value;
        std::string property_label;

        RuleElement(){};
        ~RuleElement(){};
    };
    
    struct Rule
    {
        std::string ruleCategory;
        RuleElement *condition; //if part
        RuleElement *statement; //then part or simple statement
        std::string message;

	Rule();
        ~Rule();
    };
    
    class CodeReviewRulesLoader
    {
    public:
        CodeReviewRulesLoader(std::string userName);
        virtual ~CodeReviewRulesLoader();
        
        bool loadRules();
        std::vector<Rule*> getRules();
        
    private:
        std::vector<Rule*> rules;
        std::string userName;
        
    };
    
}

#endif

