
#include <Wt/WApplication>
#include <Wt/WBreak>
#include <Wt/WContainerWidget>
#include <Wt/WPushButton>
#include <Wt/WText>
#include <Wt/WTextArea>
#include <Wt/WBootstrapTheme>
#include <Wt/WTable>
#include <Wt/WCheckBox>
#include <Wt/WGroupBox>
#include <Wt/WComboBox>
#include <Wt/WTabWidget>
#include <Wt/WStackedWidget>
#include <Wt/WDialog>
#include <Wt/WAnchor>
#include <Wt/WLineEdit>
#include <Wt/WLabel>
#include <Wt/WMessageBox>

#include <map>

#include <analyzer.h>
#include <code_attribute_reader.h>
#include <prrRule.h>
#include <crfile.h>

#include <stdio.h>

#include "include/creview_logger.h"
#include "include/creview_useraccounts.h"
#include "include/creview_rules.h"


class CodeReviewPage : public Wt::WObject
{
public:
    CodeReviewPage(Wt::WApplication *application, CodeReviewAccountPage *accountPage);
    virtual ~CodeReviewPage();
    
    Wt::WContainerWidget* CreatePage();
    void ActivatePage();
    void ReviewCode();
    std::string getCodeFileName() const;
    std::string getRuleFileName() const;
    
private:
    Wt::WApplication *application;
    Wt::WContainerWidget *pageContainer;
    Wt::WTextArea *codeArea;
    Wt::WTextArea *resultArea;
    
    CodeReviewAccountPage *accountPage;
    
    std::string codeFileName;
    std::string resultsFile;
    std::string rulesFile;
    
    //internal functions
    void CreateInputFile();
};


CodeReviewPage::CodeReviewPage(Wt::WApplication *application, CodeReviewAccountPage *accountPage)
{
    this->application = application;
    this->accountPage = accountPage;
    codeFileName.clear();
    resultsFile.clear();
    rulesFile.clear();
}


CodeReviewPage::~CodeReviewPage()
{
    if (pageContainer)
        pageContainer->clear();
}

Wt::WContainerWidget* CodeReviewPage::CreatePage()
{
    pageContainer = new Wt::WContainerWidget();
    Wt::WText *textPC = new Wt::WText("Paste code");
    textPC->setStyleClass("code-box-label");
    pageContainer->addWidget(textPC);
    pageContainer->addWidget(new Wt::WBreak());
    
    codeArea = new Wt::WTextArea();
    codeArea->setStyleClass("code-box");
    pageContainer->addWidget(codeArea);
    
    pageContainer->addWidget(new Wt::WBreak());
    Wt::WPushButton *button = new Wt::WPushButton("Review Code");
    button->setStyleClass("review-button");
    pageContainer->addWidget(button);
    pageContainer->addWidget(new Wt::WBreak());
    pageContainer->addWidget(new Wt::WBreak());
    
    resultArea = new Wt::WTextArea();
    resultArea->setStyleClass("code-box");
    pageContainer->addWidget(resultArea);
    resultArea->setReadOnly(true);
    
    button->clicked().connect(this, &CodeReviewPage::ReviewCode);
    return pageContainer;
}

void CodeReviewPage::ActivatePage()
{
    //std::cout << "CodeReviewPage::ActivatePage()" << std::endl;
}

void CodeReviewPage::CreateInputFile()
{
    //create a file containing the input code
    /*
     Extra code is added to remove the following lines from the code, as the parser is not able to get rid of them.
     (1) C++ style comments (lines starting with '//' as well as lines containing the C++ comment at the end)
     (2) Pre-processor directives (lines starting with '#')
     */
    std::string inputcodeFileName = "files/cr_input/" + application->sessionId() + "_code";
    codeFileName = "files/cr_input/" + application->sessionId();
    LOG_TRACE << "Input file - " << inputcodeFileName;
    std::ofstream codeFileOs(inputcodeFileName);
    codeFileOs << codeArea->text();
    codeFileOs.close();
    
    std::string line;
    std::ofstream codeFile(codeFileName);
    std::ifstream codeFileIs(inputcodeFileName);
    while (getline(codeFileIs,line))
    {
        if (line.find("#") != line.npos && (line.find("#") == line.find_first_not_of(" \t\r\n")))
            continue;
        if (line.find("//") != line.npos)
        {
            //C++ style comment at begininning of the line, in that case ignore entire line.
            //else take the line till the begininning of the comment
            if (line.find("//") == line.find_first_not_of(" \t\r\n"))
                continue;
            else
                line = line.substr(0, line.find("//"));
        }
        codeFile << line << std::endl;
    }
    codeFileIs.close();
    codeFile.close();
}

void CodeReviewPage::ReviewCode()
{
    try {
        LOG_TRACE << "Code review begin...";
        
        //create input file
        CreateInputFile();
        
        //parse source code
        CodeReview::CFile* cFile = CodeReview::parseSourceCode(codeFileName);
        if (cFile == NULL)
        {
            LOG_ERROR << "Parsing source code failed !";
            return;
        }
        LOG_INFO << "Successfully parsed source code file";
        
        //generate rule sets
        std::string ruleFileName = getRuleFileName();
        LOG_TRACE << "Rule file - " << ruleFileName;
        std::map<std::wstring, PRR::RuleSet*> ruleSets;
        CodeReview::generateRuleSet(ruleFileName, cFile, ruleSets);
        if (ruleSets.empty())
    	{
            LOG_ERROR << L"Failed while generating rule sets !";
       	    return;
    	}
        LOG_INFO << "Successfully generated rule sets";
        
        //review code
        resultsFile = "files/cr_output/" + application->sessionId();
        LOG_TRACE << "Results file - " << resultsFile;
        CodeReview::evaluateRuleSet(cFile,ruleSets,resultsFile);
        
        //set the result for the user to see...
        std::wifstream results(resultsFile);
        results.seekg(0, std::ios::end);
        size_t size = results.tellg();
        std::wstring resultsBuffer(size, ' ');
        results.seekg(0);
        results.read(&resultsBuffer[0], size);
        resultArea->setText(resultsBuffer);
        //results.close();
        
        
        std::map<std::wstring, PRR::RuleSet*>::iterator it = ruleSets.begin();
        while(it != ruleSets.end())
        {
            PRR::RuleSet *pRuleSet = it->second;
            ++it;
            if (pRuleSet != NULL)
                delete pRuleSet;
        }
        
        if (cFile)
            delete cFile;
        
    } catch (...) {
        codeFileName.clear();
        LOG_FATAL << "Exception while reviewing code.";
        resultArea->setText("Exception while reviewing code");
    }
}

std::string CodeReviewPage::getCodeFileName() const
{
    return codeFileName;
}

std::string CodeReviewPage::getRuleFileName() const
{
    std::string rulefile = "files/rules/default/rules";
    if (accountPage && !accountPage->getCurrentUserName().empty())
    {
        std::string userRuleFile = "files/rules/" + accountPage->getCurrentUserName();
        std::ifstream ruleFile(userRuleFile);
        if (ruleFile.good())
            rulefile = userRuleFile;
    }
    
    return rulefile;
}

/****
-- CodeRulesPage class : for constructing rules page
****/ 

class CodeRulesPage : public Wt::WObject
{
public:
    CodeRulesPage(Wt::WApplication *application, CodeReviewAccountPage *accountPage);
    virtual ~CodeRulesPage();
    
    Wt::WContainerWidget* CreatePage();
    void ActivatePage();
    
private:
    Wt::WApplication *application;
    CodeReviewAccountPage *accountPage;
    Wt::WContainerWidget *pageContainer;
    std::map<std::string, CodeReviewServer::RuleCategory*> ruleCategories;
};


CodeRulesPage::CodeRulesPage(Wt::WApplication *application, CodeReviewAccountPage *accountPage)
{
    this->application = application;
    this->accountPage = accountPage;
}


CodeRulesPage::~CodeRulesPage()
{
    if (pageContainer)
        pageContainer->clear();
}

void ShowRules(CodeReviewServer::RuleCategory *ruleCategory, Wt::WGroupBox *rulesGroupBox);
void createRule(Wt::WGroupBox *rulesGroupBox, std::map<std::string, CodeReviewServer::RuleCategory*>& ruleCategories, CodeReviewServer::Rule *loaded_rule = NULL);
void deleteRules(Wt::WGroupBox *rulesGroupBox, std::map<std::string, CodeReviewServer::RuleCategory*>& ruleCategories);
void ApplyRules(CodeReviewAccountPage *accountPage, std::map<std::string, CodeReviewServer::RuleCategory*>& ruleCategories);
void LoadRules(Wt::WGroupBox *rulesGroupBox, CodeReviewAccountPage *accountPage, std::map<std::string, CodeReviewServer::RuleCategory*>& ruleCategories);
void ShowAccountDialog(CodeReviewAccountPage *accountPage, Wt::WText *welcomeMessage);

Wt::WContainerWidget* CodeRulesPage::CreatePage()
{
    pageContainer = new Wt::WContainerWidget();
    
    Wt::WContainerWidget *leftPanelContainer = new Wt::WContainerWidget();
    Wt::WContainerWidget *rightPanelContainer = new Wt::WContainerWidget();
    leftPanelContainer->setStyleClass("rules-container-left");
    rightPanelContainer->setStyleClass("rules-container-right");
    
    Wt::WTable *rulesButtonTable = new Wt::WTable();
    Wt::WPushButton *addRule = new Wt::WPushButton("Add Rule");
    addRule->setStyleClass("rules-container-right-addrule-button");
    Wt::WPushButton *deleteRule = new Wt::WPushButton("Delete Rule");
    deleteRule->setStyleClass("rules-container-right-addrule-button");
    Wt::WPushButton *applyRules = new Wt::WPushButton("Apply & Save");
    applyRules->setStyleClass("rules-container-right-addrule-button");
    Wt::WPushButton *loadRules = new Wt::WPushButton("Load Rules");
    loadRules->setStyleClass("rules-container-right-addrule-button");
    rulesButtonTable->elementAt(0, 1)->addWidget(addRule);
    rulesButtonTable->elementAt(0, 2)->addWidget(deleteRule);
    rulesButtonTable->elementAt(0, 3)->addWidget(applyRules);
    rulesButtonTable->elementAt(0, 4)->addWidget(loadRules);
    
    // give option to persist rules for user
    //Wt::WAnchor *accountCreation = new Wt::WAnchor();
    //accountCreation->setText("Reuse the rules, by creating an account.");
    //rulesButtonTable->elementAt(0, 5)->addWidget(accountCreation);
    Wt::WPushButton *accountCreation = new Wt::WPushButton("User Account");
    accountCreation->setStyleClass("btn-link");
    accountCreation->setToolTip("Reuse the rules, by creating a user account.");
    Wt::WText *welcomeMessage = new Wt::WText("");
    accountCreation->clicked().connect(boost::bind(&ShowAccountDialog, this->accountPage, welcomeMessage));
    rulesButtonTable->elementAt(0, 5)->addWidget(accountCreation);

    
    rightPanelContainer->addWidget(rulesButtonTable);
    
    Wt::WGroupBox *ruleGroupBox = new Wt::WGroupBox("");
    ruleGroupBox->setStyleClass("rules-container-right-panel");
    rightPanelContainer->addWidget(ruleGroupBox);
    
    Wt::WStackedWidget *rulesStackWidget = new Wt::WStackedWidget();
    ruleGroupBox->addWidget(rulesStackWidget);
    
    std::vector<CodeReview::CodeAttributes*> codeAttributesList;
    CodeReview::readCodeAttributes(codeAttributesList);
    for(std::vector<CodeReview::CodeAttributes*>::iterator it = codeAttributesList.begin(); it != codeAttributesList.end(); ++it)
    {
        CodeReview::CodeAttributes *codeAttributes  = *it;
        if (codeAttributes)
        {
            CodeReviewServer::RuleCategory *ruleCategory = new CodeReviewServer::RuleCategory();
            ruleCategory->name = codeAttributes->getCodeUnitName();
            ruleCategory->button = new Wt::WPushButton(ruleCategory->name);
            ruleCategory->rulesTable = new Wt::WTable();
            ruleCategory->rulesTable->setStyleClass("table-striped");
            ruleCategory->attributes = codeAttributes;
            ruleCategories[ruleCategory->name] = ruleCategory;
            
            ruleCategory->button->setStyleClass("rule-button");
            leftPanelContainer->addWidget(ruleCategory->button);
            ruleCategory->button->clicked().connect(boost::bind(&ShowRules, ruleCategory, ruleGroupBox));
            rulesStackWidget->addWidget(ruleCategory->rulesTable);
        }
    }
    
    CodeReviewServer::Rule *loaded_rule = NULL;
    addRule->clicked().connect(boost::bind(&createRule, ruleGroupBox, ruleCategories, loaded_rule));
    deleteRule->clicked().connect(boost::bind(&deleteRules, ruleGroupBox, ruleCategories));
    applyRules->clicked().connect(boost::bind(&ApplyRules, this->accountPage, ruleCategories));
    loadRules->clicked().connect(boost::bind(&LoadRules, ruleGroupBox, this->accountPage, ruleCategories));

    pageContainer->addWidget(leftPanelContainer);
    pageContainer->addWidget(rightPanelContainer);
    
    return pageContainer;
}

void CodeRulesPage::ActivatePage()
{
    //std::cout << "CodeRulesPage::ActivatePage()" << std::endl;
}

void ShowAccountDialog(CodeReviewAccountPage *accountPage, Wt::WText *welcomeMessage)
{
    if (accountPage)
        accountPage->ShowPage(welcomeMessage);
}


void setRuleElementAlignment(Wt::WTableCell *tableCell)
{
    if (tableCell)
    {
        tableCell->setContentAlignment(Wt::AlignMiddle | Wt::AlignCenter);
        tableCell->setPadding(5);
    }
}

void codeAttributeSelected(Wt::WComboBox *attributes, std::string selectedProperty, CodeReview::CodeAttributes* codeAttributes, Wt::WTable *table, int row, int column)
{
    std::string attributeName = attributes->currentText().narrow();
    if (!attributeName.empty() && codeAttributes)
    {
        CodeReview::CodeAttributeProps *attributeProps = codeAttributes->getAttributeProperties(attributeName);
        std::map<std::string,std::string> properties = attributeProps->getProperties();
        Wt::WComboBox *attribute_properties = new Wt::WComboBox();
        attribute_properties->setStyleClass("rules-container-rule-element");
        
        //add an empty item. This is for allowing rule parts without properties. Ex: IF Class::Method = "true" THEN...
        if (!properties.empty())
            attribute_properties->addItem("");
        
        for(std::map<std::string,std::string>::iterator it = properties.begin(); it != properties.end(); ++it)
            attribute_properties->addItem(it->first);
        
        if (!selectedProperty.empty())
            attribute_properties->setValueText(selectedProperty);
        
        setRuleElementAlignment(table->elementAt(row, column));
        table->elementAt(row, column)->clear();
        table->elementAt(row, column)->addWidget(attribute_properties);
    }
}

void createARuleRow(Wt::WComboBox *ruleTypes, Wt::WTable *ruleTable, int rowCount, CodeReview::CodeAttributes *codeUnitAttributes, CodeReviewServer::Rule *loaded_rule);
void fillAttributeProperties(Wt::WComboBox *attributeProperties);

void createRule(Wt::WGroupBox *rulesGroupBox, std::map<std::string, CodeReviewServer::RuleCategory*>& ruleCategories, CodeReviewServer::Rule *loaded_rule)
{
    std::string ruleCategoryName = "";
    if (rulesGroupBox && !loaded_rule)
        ruleCategoryName = rulesGroupBox->title().narrow();
    else if (loaded_rule)
    {
        ruleCategoryName = loaded_rule->ruleCategory;
        rulesGroupBox->setTitle(ruleCategoryName);
    }
    
    if (ruleCategoryName.empty())
    {
        Wt::WMessageBox::show("Create Rule", "Select a rule category from left panel !",Wt::Ok);
        return;
    }
    
    CodeReviewServer::RuleCategory *ruleCategory = ruleCategories[ruleCategoryName];
    CodeReview::CodeAttributes *codeUnitAttributes = ruleCategory->attributes;
    if (ruleCategory == NULL || codeUnitAttributes == NULL)
    {
        LOG_ERROR << "Creating rule - generating UI widgets failed !";
        return;
    }
    
    Wt::WTable *ruleTable = ruleCategory->rulesTable;
    
    int rowCount = ruleTable->rowCount();
    
    Wt::WCheckBox *checkBox = new Wt::WCheckBox("");
    checkBox->setChecked(false);
    setRuleElementAlignment(ruleTable->elementAt(rowCount, 0));
    ruleTable->elementAt(rowCount, 0)->addWidget(checkBox);
    
    Wt::WComboBox *combo_ruleTypes = new Wt::WComboBox();
    combo_ruleTypes->setStyleClass("rules-container-rule-element");
    combo_ruleTypes->addItem("");
    combo_ruleTypes->addItem("Simple");
    combo_ruleTypes->addItem("Conditional");
    setRuleElementAlignment(ruleTable->elementAt(rowCount, 1));
    ruleTable->elementAt(rowCount, 1)->addWidget(combo_ruleTypes);
    combo_ruleTypes->activated().connect(boost::bind(&createARuleRow, combo_ruleTypes, ruleTable, rowCount, codeUnitAttributes, loaded_rule));
    
    if (loaded_rule)
    {
	combo_ruleTypes->setValueText("Simple");
        if (loaded_rule->condition && !(loaded_rule->condition->category.empty()))
	    combo_ruleTypes->setValueText("Conditional");
        createARuleRow(combo_ruleTypes, ruleTable, rowCount, codeUnitAttributes, loaded_rule);
    }
}

void fillAttributeProperties(Wt::WComboBox *attributeProperties)
{
    std::vector<std::string> attribute_property_values;
   // attribute_property_values.push_back("");
    attribute_property_values.push_back("is");
    attribute_property_values.push_back("has");
    attribute_property_values.push_back("does");
    attribute_property_values.push_back("with");
    attribute_property_values.push_back("should");
    attribute_property_values.push_back("is not");
    attribute_property_values.push_back("has not");
    attribute_property_values.push_back("does not");
    attribute_property_values.push_back("without");
    attribute_property_values.push_back("should not");
    attribute_property_values.push_back("which is");
    attribute_property_values.push_back("which is not");
    
    for(std::vector<std::string>::const_iterator it = attribute_property_values.begin(); it != attribute_property_values.end(); ++it)
        attributeProperties->addItem(*it);
}

void createARuleRow(Wt::WComboBox *ruleTypes, Wt::WTable *ruleTable, int rowCount, CodeReview::CodeAttributes *codeUnitAttributes, CodeReviewServer::Rule *loaded_rule)
{
    int column = 2;
    std::string ruleType = ruleTypes->currentText().narrow();
    if (ruleType.empty())
        return;
    
    //cannot change the rule type once a rule row is added. delete and add the rule again
    if (ruleTable->elementAt(rowCount, column)->count() > 0)
    {
        Wt::WMessageBox::show("Message", "Cannot change the rule type. Delete the rule and add a new one.", Wt::Ok);
        if (ruleType == "Simple")
            ruleTypes->setValueText("Conditional");
        else
            ruleTypes->setValueText("Simple");
        return;
    }
    
    std::vector<std::string> attributes = codeUnitAttributes->getAttributes();
    if (ruleType == "Simple")
    {
        //simple rule
        
        //create attribute combo box
        Wt::WComboBox *combo_attributes = new Wt::WComboBox();
        combo_attributes->setStyleClass("rules-container-rule-element");
        
        //fill-in values in the attribute names combo box. An empty value is added for better accessibility
        combo_attributes->setNoSelectionEnabled(true);
        combo_attributes->addItem("");
        for(std::vector<std::string>::iterator it = attributes.begin(); it != attributes.end(); ++it)
            combo_attributes->addItem(*it);
        setRuleElementAlignment(ruleTable->elementAt(rowCount, column));
        ruleTable->elementAt(rowCount, column++)->addWidget(combo_attributes);
        
        //create and fill-in values for attribute name properties
        Wt::WComboBox *combo_attribute_oper = new Wt::WComboBox();
        combo_attribute_oper->setStyleClass("rules-container-rule-element");
        fillAttributeProperties(combo_attribute_oper);
        setRuleElementAlignment(ruleTable->elementAt(rowCount, column));
        ruleTable->elementAt(rowCount, column++)->addWidget(combo_attribute_oper);
       
        //fill-in values in the attribute values
        if (!loaded_rule)
            combo_attributes->activated().connect(boost::bind(&codeAttributeSelected, combo_attributes, "", codeUnitAttributes, ruleTable, rowCount, column++));
        
        //set values incase of loaded rule
        if (loaded_rule && loaded_rule->statement)
        {
            combo_attributes->setValueText(loaded_rule->statement->attribute);
            combo_attribute_oper->setValueText(loaded_rule->statement->property_label);
            std::string attribute_property = "";
            if (!(loaded_rule->statement->property.empty()))
                attribute_property = codeUnitAttributes->getAttributeProperties(loaded_rule->statement->attribute)->getPropertyName(loaded_rule->statement->property);
            codeAttributeSelected(combo_attributes, attribute_property, codeUnitAttributes, ruleTable, rowCount, column++);
        }
    }
    else
    {
        //conditional rule
        
        Wt::WText *textIf = new Wt::WText("If");
        textIf->setStyleClass("rules-container-rule-element");
        setRuleElementAlignment(ruleTable->elementAt(rowCount, column));
        ruleTable->elementAt(rowCount, column++)->addWidget(textIf);
    
        Wt::WComboBox *combo_attributes_left = new Wt::WComboBox();
        Wt::WComboBox *combo_attributes_right = new Wt::WComboBox();
        combo_attributes_left->setStyleClass("rules-container-rule-element");
        combo_attributes_right->setStyleClass("rules-container-rule-element");
        std::vector<std::string> attributes = codeUnitAttributes->getAttributes();
    
        //add empty items for left and right attribute selection combo-boxes. This is for better accessibility
        combo_attributes_left->addItem("");
        combo_attributes_right->addItem("");
        for(std::vector<std::string>::iterator it = attributes.begin(); it != attributes.end(); ++it)
        {
            combo_attributes_left->addItem(*it);
            combo_attributes_right->addItem(*it);
        }
        combo_attributes_left->setNoSelectionEnabled(true);
        setRuleElementAlignment(ruleTable->elementAt(rowCount, column));
        ruleTable->elementAt(rowCount, column++)->addWidget(combo_attributes_left);
    
        Wt::WComboBox *combo_attribute_oper_left = new Wt::WComboBox();
        Wt::WComboBox *combo_attribute_oper_right = new Wt::WComboBox();
        combo_attribute_oper_left->setStyleClass("rules-container-rule-element");
        combo_attribute_oper_right->setStyleClass("rules-container-rule-element");
        fillAttributeProperties(combo_attribute_oper_left);
        fillAttributeProperties(combo_attribute_oper_right);
        setRuleElementAlignment(ruleTable->elementAt(rowCount, column));
        ruleTable->elementAt(rowCount, column++)->addWidget(combo_attribute_oper_left);
       
        int attributes_left_column_no = column; 
        if (!loaded_rule)
            combo_attributes_left->activated().connect(boost::bind(&codeAttributeSelected, combo_attributes_left, "", codeUnitAttributes, ruleTable, rowCount, column));
        
        column += 1;
        Wt::WText *textThen = new Wt::WText("Then");
        textThen->setStyleClass("rules-container-rule-element");
        setRuleElementAlignment(ruleTable->elementAt(rowCount, column));
        ruleTable->elementAt(rowCount, column++)->addWidget(textThen);
    
        setRuleElementAlignment(ruleTable->elementAt(rowCount, column));
        ruleTable->elementAt(rowCount, column++)->addWidget(combo_attributes_right);
        setRuleElementAlignment(ruleTable->elementAt(rowCount, column));
        ruleTable->elementAt(rowCount, column++)->addWidget(combo_attribute_oper_right);
    
	    int attributes_right_column_no = column;
        if (!loaded_rule)
            combo_attributes_right->activated().connect(boost::bind(&codeAttributeSelected, combo_attributes_right, "", codeUnitAttributes, ruleTable, rowCount, column));
        column += 1;
        
        //set left attribute value from loaded rule if exists
        if (loaded_rule && loaded_rule->condition && loaded_rule->statement)
        {
            combo_attributes_left->setValueText(loaded_rule->condition->attribute);
            combo_attributes_right->setValueText(loaded_rule->statement->attribute);
            combo_attribute_oper_left->setValueText(loaded_rule->condition->property_label);
            combo_attribute_oper_right->setValueText(loaded_rule->statement->property_label);
            
            std::string attribute_left_property = "";
            if (!(loaded_rule->condition->property.empty()))
                attribute_left_property = codeUnitAttributes->getAttributeProperties(loaded_rule->condition->attribute)->getPropertyName(loaded_rule->condition->property);
	    codeAttributeSelected(combo_attributes_left,attribute_left_property,codeUnitAttributes, ruleTable, rowCount, attributes_left_column_no);

            std::string attribute_right_property = "";
            if (!(loaded_rule->statement->property.empty()))
                attribute_right_property = codeUnitAttributes->getAttributeProperties(loaded_rule->statement->attribute)->getPropertyName(loaded_rule->statement->property);
	    codeAttributeSelected(combo_attributes_right,attribute_right_property,codeUnitAttributes, ruleTable, rowCount, attributes_right_column_no);
        }
    }
    
    Wt::WLabel *messageLabel = new Wt::WLabel("  Message: ");
    Wt::WTextArea *messageText = new Wt::WTextArea("");
    messageText->setRows(1);
    messageLabel->setBuddy(messageText);
    setRuleElementAlignment(ruleTable->elementAt(rowCount, column));
    ruleTable->elementAt(rowCount, column++)->addWidget(messageLabel);
    setRuleElementAlignment(ruleTable->elementAt(rowCount, column));
    ruleTable->elementAt(rowCount, column)->addWidget(messageText);
    if (loaded_rule)
        messageText->setText(loaded_rule->message);
}


void deleteRules(Wt::WGroupBox *rulesGroupBox, std::map<std::string, CodeReviewServer::RuleCategory*>& ruleCategories)
{
    std::string ruleCategoryName = rulesGroupBox->title().narrow();
    if (ruleCategoryName.empty())
        return;
    
    CodeReviewServer::RuleCategory *ruleCategory = ruleCategories[ruleCategoryName];
    if (ruleCategory == NULL)
    {
        LOG_ERROR << "deleteRule. Invalid rule category !";
        return;
    }

    Wt::WTable *rulesTable = ruleCategory->rulesTable;
    for(unsigned int index = 0; index < rulesTable->rowCount(); index++)
    {
        Wt::WCheckBox *selectedRule = (Wt::WCheckBox*)rulesTable->elementAt(index, 0)->widget(0);
        if (selectedRule->checkState() == Wt::Checked)
        {
            //delete each widget in the row and finally delete the row
            int column = 0;
            while(rulesTable->elementAt(index, column)->count() > 0)
            {
                Wt::WWidget *ruleWidget = rulesTable->elementAt(index, column)->widget(0);
                if (ruleWidget)
                    delete ruleWidget;
            }
            
            rulesTable->deleteRow(index--);
        }
    }
}


void ShowRules(CodeReviewServer::RuleCategory *ruleCategory, Wt::WGroupBox *rulesGroupBox)
{
    if (ruleCategory && ruleCategory->rulesTable)
    {
        Wt::WStackedWidget *stackedWidget = (Wt::WStackedWidget*)rulesGroupBox->widget(0);
        rulesGroupBox->setTitle(ruleCategory->name);
        stackedWidget->setCurrentWidget(ruleCategory->rulesTable);
    }
}

void ApplyRules(CodeReviewAccountPage *accountPage, std::map<std::string, CodeReviewServer::RuleCategory*>& ruleCategories)
{
    if (!accountPage || accountPage->getCurrentUserName().empty())
    {
        LOG_ERROR << "ApplyRules. Invalid session !";
        return;
    }
    
    CodeReviewServer::CodeReviewRulesPersister creviewPersister(accountPage->getCurrentUserName());
    creviewPersister.persistRules(ruleCategories);
}

void LoadRules(Wt::WGroupBox *rulesGroupBox, CodeReviewAccountPage *accountPage, std::map<std::string, CodeReviewServer::RuleCategory*>& ruleCategories)
{
    if (!accountPage || accountPage->getCurrentUserName().empty())
    {
        LOG_ERROR << "LoadRules. Invalid session !";
        return;
    }
    
    CodeReviewServer::CodeReviewRulesLoader *creviewRulesLoader = new CodeReviewServer::CodeReviewRulesLoader(accountPage->getCurrentUserName());
    if (creviewRulesLoader->loadRules())
    {
        std::vector<CodeReviewServer::Rule*> loaded_rules = creviewRulesLoader->getRules();
        //for each loaded rule - create the UI elements
        for (std::vector<CodeReviewServer::Rule*>::const_iterator it = loaded_rules.begin(); it != loaded_rules.end(); ++it)
        {
            CodeReviewServer::Rule *rule = *it;
            if (rule)
                createRule(rulesGroupBox, ruleCategories, rule);
        }
    }

    if (creviewRulesLoader)
        delete creviewRulesLoader;
}


class CodeReviewer : public Wt::WApplication
{
public:
    CodeReviewer(const Wt::WEnvironment& env);

private:
    CodeReviewPage *codeReviewPage;
    CodeRulesPage *codeRulesPage;
    CodeReviewAccountPage *accountPage;
    Wt::WTabWidget *pages;

    void PageActivated(int index);
};


CodeReviewer::CodeReviewer(const Wt::WEnvironment& env)
    : Wt::WApplication(env)
{
    //set theme
    Wt::WBootstrapTheme *bsTheme = new Wt::WBootstrapTheme();
    bsTheme->setVersion(Wt::WBootstrapTheme::Version3);
    setTheme(bsTheme);
    
    //use style-sheet
    useStyleSheet("resources/styles/crvstlye.css");

    setTitle("Review your code");
    
    //container widgets for different tab pages
    accountPage = new CodeReviewAccountPage(this);
    codeReviewPage = new CodeReviewPage(this, accountPage);
    codeRulesPage = new CodeRulesPage(this, accountPage);

    pages = new Wt::WTabWidget(root());
    pages->addTab(codeReviewPage->CreatePage(), "Code");
    pages->addTab(codeRulesPage->CreatePage(), "Rules");
    pages->currentChanged().connect(this, &CodeReviewer::PageActivated);

}

void CodeReviewer::PageActivated(int index)
{
    Wt::WString tabText = pages->tabText(index);
    if (tabText == "Code" && codeReviewPage)
        codeReviewPage->ActivatePage();
}

Wt::WApplication *createApplication(const Wt::WEnvironment& env)
{
    return new CodeReviewer(env);
}


int main(int argc, char **argv)
{
    return Wt::WRun(argc, argv, &createApplication);
}
