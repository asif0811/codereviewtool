#include <iostream>

#include <Wt/WApplication>
#include <Wt/WText>
#include <Wt/WBreak>
#include <Wt/WPushButton>
#include <Wt/WString>
#include <Wt/WMessageBox>
#include <Wt/WLineEdit>

#include <sqlite3.h>

#include "include/creview_logger.h"
#include "include/creview_rules.h"
#include "include/creview_useraccounts.h"


CodeReviewAccountPage::CodeReviewAccountPage(Wt::WApplication *application)
{
    this->application = application;
    accountDlg = new Wt::WDialog("Account Information");
    CreatePage();
}

CodeReviewAccountPage::~CodeReviewAccountPage()
{
    if (accountDlg->contents())
        accountDlg->contents()->clear();
    
    if (accountDlg)
        delete accountDlg;
}

void UserLogin(CodeReviewAccountPage *accountPage, std::string sessionId);
void AddNewUser(CodeReviewAccountPage *accountPage, std::string sessionId);
void CodeReviewAccountPage::CreatePage()
{
    accountDlg->titleBar()->setContentAlignment(Wt::AlignCenter);
    accountDlg->setResizable(true);
    accountDlg->setClosable(true);
    accountDlg->contents()->setContentAlignment(Wt::AlignCenter);
    accountDlg->contents()->setStyleClass("account-dialog");
    
    new Wt::WText("Reuse the rules, with a user account.", accountDlg->contents());
    new Wt::WBreak(accountDlg->contents());
    new Wt::WBreak(accountDlg->contents());
    
    new Wt::WText("Name", accountDlg->contents());
    userName = new Wt::WLineEdit("", accountDlg->contents());
    //userName->setStyleClass("account-dialog-edit");
    new Wt::WBreak(accountDlg->contents());
    
    new Wt::WText("Password", accountDlg->contents());
    password = new Wt::WLineEdit("", accountDlg->contents());
    //password->setStyleClass("account-dialog-edit");
    password->setEchoMode(Wt::WLineEdit::Password);
    new Wt::WBreak(accountDlg->contents());
    welcomeMessage = new Wt::WText("", accountDlg->contents());
    welcomeMessage->setStyleClass("account-dialog-welcometext");
    new Wt::WBreak(accountDlg->contents());
    
    Wt::WPushButton *loginButton = new Wt::WPushButton("Login", accountDlg->contents());
    loginButton->setStyleClass("account-dialog-button");
    new Wt::WBreak(accountDlg->contents());
    new Wt::WBreak(accountDlg->contents());
    Wt::WPushButton *newUserButton = new Wt::WPushButton("Add New User", accountDlg->contents());
    newUserButton->setStyleClass("account-dialog-button");
    loginButton->clicked().connect(boost::bind(&UserLogin, this, application->sessionId()));
    newUserButton->clicked().connect(boost::bind(&AddNewUser, this, application->sessionId()));
}

void CodeReviewAccountPage::ShowPage(Wt::WText *welcomeMessage)
{
    //this->welcomeMessage = welcomeMessage;
    
    //user is already logged in
    if (userSessions.count(application->sessionId()) > 0) {
        accountDlg->contents()->setDisabled(true);
        std::cout << std::endl << "Session id: " << application->sessionId() << " , user: " << userSessions[application->sessionId()];
    }
    else
        accountDlg->contents()->setDisabled(false);
    
    setWelcomeMessage();
    accountDlg->show();
}

void CodeReviewAccountPage::setWelcomeMessage()
{
    Wt::WString welcomeText = "";
    if (userSessions.count(application->sessionId()) > 0)
    {
        welcomeText = userSessions[application->sessionId()] + " logged in.";
        /*CodeReviewServer::CodeReviewRulesLoader rulesLoader(userSessions[application->sessionId()]);
        if (rulesLoader.loadRules())
        {
            std::cout << std::endl << "User : " << userSessions[application->sessionId()] << " has rules.";
        }
        else
        {
            std::cout << std::endl << "User : " << userSessions[application->sessionId()] << " does not have rules.";
        }*/
    }
    
    if (welcomeMessage)
        welcomeMessage->setText(welcomeText);
}

void CodeReviewAccountPage::addUserSession(std::string sessionId, std::string userName)
{
    userSessions[sessionId] = userName;
}

int dbCallback(void *NotUsed, int argc, char **argv, char **azColName)
{
    if (NotUsed != NULL)
    {
        int *returnedRows = (int*)(NotUsed);
        *returnedRows = *returnedRows + 1;
    }
    return 0;
}

void UserLogin(CodeReviewAccountPage *accountPage, std::string sessionId)
{
    std::string userName = accountPage->getCurrentUserName();
    std::string password = accountPage->getCurrentUserPassword();
    
    //open database connection
    sqlite3 *db = NULL;
    
    //open database connection
    int rc = sqlite3_open("./database/auth.db", &db);
    if (rc)
    {
        LOG_ERROR << "Cannot open database !";
        Wt::WMessageBox::show("User Login", "No database connection !",Wt::Ok);
        return;
    }
    
    //authenticate to the database as admin user
    std::string adminUser = "asif";
    std::string adminPassword = "kottayam";
    rc = sqlite3_user_authenticate(db,adminUser.c_str(),adminPassword.c_str(),adminPassword.length());
    if (rc != SQLITE_OK)
    {
        LOG_ERROR << "Authentication failed for database !";
        Wt::WMessageBox::show("User Login", "Authentication failed !",Wt::Ok);
        sqlite3_close(db);
        return;
    }
    
    //check if user already exists in the database
    std::string sqlstmt1 = std::string("SELECT NAME FROM USERS WHERE NAME = '" + userName + std::string("' AND PASSWORD = '") + password + std::string("'"));
    std::cout << std::endl << sqlstmt1;
    char *errMsg = NULL;
    int *returnedRows = (int*)malloc(sizeof(int));
    *returnedRows = 0;
    rc = sqlite3_exec(db, sqlstmt1.c_str(), dbCallback, returnedRows, &errMsg);
    if (rc != SQLITE_OK || ((*returnedRows) == 0))
    {
        LOG_ERROR << "Invalid user or password";
        Wt::WMessageBox::show("User Login", "Invalid User or password. !",Wt::Ok);
        if (errMsg != NULL)
        {
            LOG_ERROR << "SQL Error: " << errMsg;
            sqlite3_free(errMsg);
        }
        if (returnedRows != NULL)
            free(returnedRows);
        sqlite3_close(db);
        return;
    }
    
    if (db)
        sqlite3_close(db);
    
    if (returnedRows)
        free(returnedRows);
    
    accountPage->addUserSession(sessionId,userName);
    accountPage->setWelcomeMessage();
    accountPage->closeDialog();
}

void AddNewUser(CodeReviewAccountPage *accountPage, std::string sessionId)
{
    std::string userName = accountPage->getCurrentUserName();
    std::string password = accountPage->getCurrentUserPassword();
    
    if (userName.empty() || password.empty())
    {
        Wt::WMessageBox::show("Add New User", "Empty user name or password !",Wt::Ok);
        return;
    }
    
    sqlite3 *db = NULL;
    //open database connection
    int rc = sqlite3_open("./database/auth.db", &db);
    if (rc)
    {
        LOG_ERROR << "Cannot open database !";
        Wt::WMessageBox::show("User Login", "No database connection !",Wt::Ok);
        return;
    }
    
    //authenticate to the database as admin user
    std::string adminUser = "asif";
    std::string adminPassword = "kottayam";
    rc = sqlite3_user_authenticate(db,adminUser.c_str(),adminPassword.c_str(),adminPassword.length());
    if (rc != SQLITE_OK)
    {
        LOG_ERROR << "Authentication failed for database !";
        Wt::WMessageBox::show("User Login", "Authentication failed !",Wt::Ok);
        sqlite3_close(db);
        return;
    }
    
    //check if user already exists in the database
    std::string dummySessionId = "dummy";
    std::string description = "descr";
    std::string sqlstmt1 = std::string("INSERT INTO USERS (NAME,PASSWORD,SESSIONID,DESCRIPTION) VALUES ('") + userName + std::string("','") + password + std::string("','") + dummySessionId + std::string("','") + description + std::string("')");
    char *errMsg = NULL;
    rc = sqlite3_exec(db, sqlstmt1.c_str(), NULL, NULL, &errMsg);
    if (rc != SQLITE_OK)
    {
        LOG_ERROR << "SQL Error: " << errMsg;
        Wt::WMessageBox::show("Add New User", "User already exists. !",Wt::Ok);
        sqlite3_free(errMsg);
        sqlite3_close(db);
        return;
    }
    
    if (db)
        sqlite3_close(db);
    
    Wt::WMessageBox::show("User Login", "User creation success",Wt::Ok);
    
    accountPage->addUserSession(sessionId,userName);
    accountPage->setWelcomeMessage();
    accountPage->closeDialog();
}
