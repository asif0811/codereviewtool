#include <fstream>
#include <algorithm>

#include <Wt/WComboBox>
#include <Wt/WTextArea>
#include <Wt/WPushButton>
#include <Wt/WMessageBox>

#include <code_attribute_reader.h>

#include "include/creview_rules.h"
#include "include/creview_logger.h"
#include "uiRuleLexer.hpp"
#include "uiRuleParser.hpp"

namespace CodeReviewServer
{
    CodeReviewRulesPersister::CodeReviewRulesPersister(std::string userName)
    {
        this->userName = userName;
    }

    CodeReviewRulesPersister::~CodeReviewRulesPersister()
    {}

    bool CodeReviewRulesPersister::persistRules(std::map<std::string, RuleCategory*>& ruleCategories)
    {
        bool status = false;
        if (userName.empty())
        {
            LOG_ERROR << "CodeReviewRulesPersister: Invalid user name";
            return status;
        }
        if (ruleCategories.empty())
        {
            LOG_ERROR << "CodeReviewRulesPersister: No rules to persist for user: " << userName;
            return status;
        }
    
        //create and open the rules file
        std::string rulesFileName = "files/rules/" + userName;
        LOG_TRACE << "CodeReviewRulesPersister: Rules file :" << rulesFileName;
        std::ofstream rulesFile(rulesFileName);
    
        //write rules to file
        for(std::map<std::string, RuleCategory*>::iterator it = ruleCategories.begin(); it != ruleCategories.end(); ++it)
        {
            std::string ruleCategory = it->first;
            Wt::WTable* rulesInTable = (it->second)->rulesTable;
            CodeReview::CodeAttributes *categoryAttributes = (it->second)->attributes;
            status = writeRulestoFile(rulesFile,ruleCategory,rulesInTable, categoryAttributes);
            if (!status)
            {
                LOG_ERROR << "CodeReviewRulesPersister: Error while writing rules to file : " << rulesFileName;
                break;
            }
        }
    
        rulesFile.close();
        return status;
    }

    bool CodeReviewRulesPersister::writeRulestoFile(std::ofstream& rulesFile, std::string ruleCategory, Wt::WTable* rulesInTable,
                                                CodeReview::CodeAttributes *categoryAttributes)
    {
        bool status = true;
        try
        {
            if (!rulesInTable || rulesInTable->rowCount() == 0 || !categoryAttributes)
                return status;
            
            //write rules
            //write all simple rules, followed by conditional rules
            for(int rowCount = 0; rowCount < rulesInTable->rowCount(); rowCount++)
            {
                std::string attributeName1, attributeName2, propertyName1, propertyName2;
                std::string propertyNameInRuleEngine1, propertyNameInRuleEngine2, operatorText1, operatorText2, message;
                //Category name
                std::string ruleType = getComboBoxValue(rulesInTable->elementAt(rowCount, 1)->widget(0));
                if (ruleType == "Simple")
                {
                    attributeName1 = getComboBoxValue(rulesInTable->elementAt(rowCount, 2)->widget(0));
                    propertyName1 = getComboBoxValue(rulesInTable->elementAt(rowCount, 4)->widget(0));
                    if (attributeName1.empty() || propertyName1.empty())
                        continue;
                    
                    propertyNameInRuleEngine1 = categoryAttributes->getAttributeProperties(attributeName1)->getPropertyLabel(propertyName1);
                    operatorText1 = getComboBoxValue(rulesInTable->elementAt(rowCount, 3)->widget(0));
                    message = ((Wt::WTextArea*)(rulesInTable->elementAt(rowCount, 6)->widget(0)))->text().toUTF8();
                    
                    rulesFile << ruleCategory << ":";
                    rulesFile << ruleCategory << "::" << attributeName1 << "." << propertyNameInRuleEngine1;
                    rulesFile << " " << getOperator(operatorText1) << "#" << operatorText1 << "#";
                    rulesFile << " : ERROR_MESSAGE = \"" << message << "\"" << std::endl;
                }
            }
            
            for(int rowCount = 0; rowCount < rulesInTable->rowCount(); rowCount++)
            {
                std::string attributeName1, attributeName2, propertyName1, propertyName2;
                std::string propertyNameInRuleEngine1, propertyNameInRuleEngine2, operatorText1, operatorText2, message;
                //Category name
                std::string ruleType = getComboBoxValue(rulesInTable->elementAt(rowCount, 1)->widget(0));
                if (ruleType == "Conditional")
                {
                    //IF Part
                    attributeName1 = getComboBoxValue(rulesInTable->elementAt(rowCount, 3)->widget(0));
                    propertyName1 = getComboBoxValue(rulesInTable->elementAt(rowCount, 5)->widget(0));
                    attributeName2 = getComboBoxValue(rulesInTable->elementAt(rowCount, 7)->widget(0));
                    propertyName2 = getComboBoxValue(rulesInTable->elementAt(rowCount, 9)->widget(0));
                    
                    if (attributeName1.empty() || propertyName1.empty() || attributeName2.empty() || propertyName2.empty())
                        continue;

                    propertyNameInRuleEngine1 = categoryAttributes->getAttributeProperties(attributeName1)->getPropertyLabel(propertyName1);
                    operatorText1 = getComboBoxValue(rulesInTable->elementAt(rowCount, 4)->widget(0));
                    
                    //THEN Part
                    propertyNameInRuleEngine2 = categoryAttributes->getAttributeProperties(attributeName2)->getPropertyLabel(propertyName2);
                    operatorText2 = getComboBoxValue(rulesInTable->elementAt(rowCount, 8)->widget(0));
                    message = ((Wt::WTextArea*)(rulesInTable->elementAt(rowCount, 11)->widget(0)))->text().toUTF8();
                    
                    rulesFile << ruleCategory << ":";
                    rulesFile << "IF " << ruleCategory << "::" << attributeName1 << "." << propertyNameInRuleEngine1;
                    rulesFile << " " << getOperator(operatorText1) << "#" << operatorText1 << "#";
                    rulesFile << " THEN " << ruleCategory << "::" << attributeName2 << "." << propertyNameInRuleEngine2;
                    rulesFile << " " << getOperator(operatorText2) << "#" << operatorText2 << "#";
                    rulesFile << " : ERROR_MESSAGE = \"" << message << "\"" << std::endl;
                }
            }
        }
        catch (...)
        {
            status = false;
        }
    
        return status;
    }

    std::string CodeReviewRulesPersister::getComboBoxValue(Wt::WWidget* widget)
    {
        std::string value;
        if (widget)
        {
            Wt::WComboBox *combobox = (Wt::WComboBox*)widget;
            if (combobox)
                value = combobox->currentText().toUTF8();
        }
        
        return value;
    }

    std::string CodeReviewRulesPersister::getOperator(std::string operatorText)
    {
        std::string oper;
        if (operatorText.empty() || operatorText == "is" || operatorText == "has" || operatorText == "does" || operatorText == "with" || operatorText == "should" || operatorText == "which is")
            oper = "= \"true\"";
            //oper = "<> \"false\"";
        else if (operatorText == "is not" || operatorText == "has not" || operatorText == "does not" || operatorText == "without" ||
                 operatorText == "should not" || operatorText == "which is not")
            oper ="= \"false\"";
            //oper ="<> \"true\"";
    
        return oper;
    }

    //Rule constructor - representing a single rule
    Rule::Rule()
    {
    	condition = NULL;
	    statement = NULL;
    }

    Rule::~Rule()
    {
	    if (condition)
	        delete condition;
        if (statement)
            delete statement;
    }
    
    CodeReviewRulesLoader::CodeReviewRulesLoader(std::string userName)
    {
        this->userName = userName;
    }
    
    CodeReviewRulesLoader::~CodeReviewRulesLoader()
    {
    	std::vector<Rule*>::iterator it = rules.begin();
    	while(it != rules.end())
    	{
            Rule *rule = *it;
            ++it;
            if (rule != NULL)
                delete rule;
    	}	
    }
    
    bool CodeReviewRulesLoader::loadRules()
    {
        bool status = false;
        if (userName.empty())
        {
            LOG_ERROR << "CodeReviewRulesLoader: Invalid user name";
            Wt::WMessageBox::show("Message", "User not logged in !.", Wt::Ok);
            return status;
        }
        
        std::string rulesFileName = "files/rules/" + userName;
        std::ifstream rulesFile(rulesFileName);
        if (!rulesFile.good())
        {
            LOG_ERROR << "CodeReviewRulesLoader: No rules file present for the user: " << userName;
            Wt::WMessageBox::show("Message", "Current user has no rules defined !.", Wt::Ok);
            return status;
        }
        
        //create and open the rules file
        LOG_TRACE << "CodeReviewRulesLoader: Rules file :" << rulesFileName;
        try
        {
            std::string rule_text;
            while(std::getline(rulesFile, rule_text) && !rule_text.empty())
            {
                std::cout << std::endl << "Rule === " << rule_text;
                ANTLR_UINT8* rulestatement = (ANTLR_UINT8*)rule_text.c_str();
                UIRuleParser::uiRuleLexer::InputStreamType input(rulestatement,ANTLR_ENC_8BIT,(ANTLR_UINT32)strlen(rule_text.c_str()),(ANTLR_UINT8*)"rulestmnt");
                UIRuleParser::uiRuleLexer lxr(&input);
                UIRuleParser::uiRuleParser::TokenStreamType tstream(ANTLR_SIZE_HINT, lxr.get_tokSource() );
                UIRuleParser::uiRuleParser psr(&tstream);
                    
                CodeReviewServer::Rule *uiRule = psr.rule();
                if (uiRule && uiRule->statement && !(uiRule->statement->category.empty()))
                {
                    uiRule->message.erase(remove(uiRule->message.begin(), uiRule->message.end(), '\"' ),uiRule->message.end());
                    if (!(uiRule->statement->property_value.empty()))
                        uiRule->statement->property_value.erase(remove(uiRule->statement->property_value.begin(), uiRule->statement->property_value.end(), '\"' ),uiRule->statement->property_value.end());
                    if (uiRule->condition && !(uiRule->condition->property_value.empty()))
                        uiRule->condition->property_value.erase(remove(uiRule->condition->property_value.begin(), uiRule->condition->property_value.end(), '\"' ),uiRule->condition->property_value.end());
                    
                    rules.push_back(uiRule);
                    status = true;
                }
                rule_text = "";
            }
        }
        catch(...)
        {
            std::string errorstring = "Error while parsing the rules from rule file : " + rulesFileName;
            std::cout << std::endl << errorstring;
            status = false;
        }
        
        return status;
    }
    
    std::vector<Rule*> CodeReviewRulesLoader::getRules()
    {
        return rules;
    }

}
