#ifndef _CRAN_RULESETGEN
#define _CRAN_RULESETGEN

#include <string>
#include <map>

//forward declarations
namespace PRR
{
class RuleSet;
}

namespace CodeReview
{
    //forward declarations
    class CFile;
    
    /* generate rule set
    rulesFileName - input file name containing the rules
    codeFile - parsed grammar for the code */
    void generateRuleSet(std::string rulesFileName, CFile *codeFile, std::map<std::wstring, PRR::RuleSet*>& ruleSets);
    
    //parse source code
    CFile* parseSourceCode(std::string fileName);
    
    //evaluate rule set
    void evaluateRuleSet(CFile* codeFile, std::map<std::wstring, PRR::RuleSet*>& ruleSets, std::string outFileName = std::string(""));
}

#endif
