#ifndef _CRAN_CODEATTRRDR
#define _CRAN_CODEATTRRDR

#include <string>
#include <vector>
#include <map>
#include <json/json.h>

//Functions to read code_attributes.json file
namespace CodeReview
{
    //Properties of a Code attribute
    class CodeAttributeProps
    {
        std::string attributeName;
        std::map<std::string,std::string> properties;
        std::map<std::string,std::string> property_labels;
        
    public:
        CodeAttributeProps(std::string attributeName);
        std::map<std::string,std::string> getProperties() { return properties; }
        std::string getPropertyLabel(std::string property_name);
        std::string getPropertyName(std::string property_label);
        
    friend class CodeAttributes;
    };
    
    
    //Code attributes class
    class CodeAttributes
    {
        std::string codeUnitName;
        std::vector<std::string> attributes;
        std::map<std::string, CodeAttributeProps*> attributeProps;
        
    public:
        CodeAttributes(std::string codeUnitName);
        std::string getCodeUnitName() { return codeUnitName; }
        std::vector<std::string> getAttributes() { return attributes; }
        CodeAttributeProps* getAttributeProperties(const std::string attribute);
        
        bool readAttributes(const Json::Value& codeUnit);
    };
    
    void readCodeAttributes(std::vector<CodeAttributes*>& codeAttributes);
    
}

#endif
