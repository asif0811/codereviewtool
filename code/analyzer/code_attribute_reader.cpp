#include <iostream>
#include <fstream>

#include "code_attribute_reader.h"


namespace CodeReview
{

    CodeAttributes::CodeAttributes(std::string codeUnitName)
    {
        this->codeUnitName = codeUnitName;
    }
    
    bool CodeAttributes::readAttributes(const Json::Value& codeUnit)
    {
        bool readSuccess = false;
        try
        {
            Json::Value defaultValue;
            unsigned int index = 0;
            //read attributes
            const Json::Value code_attributes = codeUnit["attributes"];
            for(index = 0; index < code_attributes.size(); ++index)
            {
                std::string attributeName = code_attributes[index].asString();
                this->attributes.push_back(attributeName);
            }
            
            //read attribute properties
            for(std::vector<std::string>::iterator it = this->attributes.begin(); it != this->attributes.end(); ++it)
            {
                CodeAttributeProps *codeAttributeProps = new CodeAttributeProps(*it);
                const Json::Value code_attribute = codeUnit[*it];
                const Json::Value code_attribute_property_names = code_attribute["properties"];
                const Json::Value code_attribute_property_values = code_attribute["values"];
                const Json::Value code_attribute_property_labels = code_attribute["labels"];
                for(index = 0; index < code_attribute_property_names.size(); ++index)
                {
                    std::string property_name = code_attribute_property_names[index].asString();
                    std::string property_value = code_attribute_property_values[index].asString();
                    std::string property_label = code_attribute_property_labels[index].asString();
                    codeAttributeProps->properties[property_name] = property_value;
                    codeAttributeProps->property_labels[property_name] = property_label;
                }
                this->attributeProps[*it] = codeAttributeProps;
                readSuccess = true;
            }
        }
        catch (...)
        {
            std::wcout << std::endl << L"Exception while reading code attributes";
        }
        
        return readSuccess;
    }
    
    CodeAttributeProps* CodeAttributes::getAttributeProperties(const std::string attribute)
    {
        if (this->attributeProps.count(attribute) == 0)
            return NULL;
        
        return this->attributeProps[attribute];
    }
    
    CodeAttributeProps::CodeAttributeProps(std::string attributeName)
    {
        this->attributeName = attributeName;
    }
    
    std::string CodeAttributeProps::getPropertyLabel(std::string property_name)
    {
        return this->property_labels[property_name];
    }
    
    std::string CodeAttributeProps::getPropertyName(std::string property_label)
    {
        std::string propertyLabel, propertyName = "";
        for(std::map<std::string,std::string>::const_iterator it = this->property_labels.begin(); it != this->property_labels.end(); ++it)
        {
            propertyLabel = it->second;
            if (propertyLabel == property_label)
            {
                propertyName = it->first;
                break;
            }
        }
        return propertyName;
    }
    
    void readCodeAttributes(std::vector<CodeAttributes*>& codeAttributesList)
    {
        try
        {
            Json::Value root;
            Json::Reader reader;
            
            std::ifstream file("code_attributes.json");
            if (!reader.parse(file, root, true))
            {
                std::cout  << "Failed to parse configuration\n" << reader.getFormattedErrorMessages();
                return;
            }
            
            Json::Value defaultValue;
            const Json::Value codeUnits = root["code-units"];
            std::cout  << "No of code units " << codeUnits.size() << std::endl;
            for(unsigned int index = 0; index < codeUnits.size(); ++index)
            {
                //Json::Value codeUnit = codeUnits[index];
                //std::vector<std::string> names = codeUnit.getMemberNames();
                //for (std::vector<std::string>::iterator it = names.begin(); it != names.end(); ++it)
                //    std::cout << *it << std::endl;
                
                std::string name = codeUnits[index].asString();
                //std::cout  << "Code unit name: " << name << std::endl;
                if (!name.empty())
                {
                    const Json::Value codeUnit = root[name];
                    CodeAttributes *codeAttributes = new CodeAttributes(name);
                    if (codeAttributes->readAttributes(codeUnit))
                        codeAttributesList.push_back(codeAttributes);
                }
            }
        }
        catch (...)
        {
            std::wcout << std::endl << L"Exception while reading code attributes";
        }
    }
}
