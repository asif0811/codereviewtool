#include <iostream>
#include <fstream>
#include <algorithm>
#include <set>
#include <grammar.h>
#include <prrRule.h>
#include <ruleengine.h>

#include "analyzer.h"
#include "crvclass.h"
#include "crvfunction.h"
#include "crnamespace.h"

namespace CodeReview
{

/***

The input rules are added into a file named rules.txt
This function reads the file, generate rules and adds them to the ruleset.
The file should be of the following form

Class
IF Class::Name STARTS_WITH "PTT" THEN Class::Type = "template class"
IF Class::Name STARTS_WITH "PTE" THEN Class::Type = "exported class"
IF Class::Name STARTS_WITH "PTD" THEN Class::Type = "derived class"
***

Function
...
...
***

***/

void generateRuleSet(std::string rulesFileName, CFile *codeFile, std::map<std::wstring, PRR::RuleSet*>& ruleSets)
	{
	try
		{
            std::set<std::wstring> ruleSetCategories;
            
            std::wcout << std::endl << std::endl << L"Generating rule sets";
            //get code file unit types (rule categories)
            for(std::list<CCodeUnit*>::iterator it = codeFile->codeUnits.begin(); it != codeFile->codeUnits.end(); ++it)
            {
                CCodeUnit* codeUnit = *it;
                if (codeUnit != NULL)
                    ruleSetCategories.insert(codeUnit->getType());
            }
            
            std::ifstream rulesfile(rulesFileName);
            std::string category, ruletext, error;
            while(rulesfile.good())
            {
                try
                {
                    std::getline(rulesfile, ruletext);
                    if (ruletext.empty())
                        break;
                    
                    error.clear();
                    category = ruletext.substr(0,ruletext.find(":"));
                    ruletext = ruletext.substr(ruletext.find(":") + 1);
                    std::wstring wCategory(category.begin(),category.end());
                    
                    if(!ruleSets.count(wCategory))
                        ruleSets[wCategory] = new PRR::RuleSet();
                    
                    std::wcout << std::endl << L"..Generating rule";
                    PRR::Rule *rule = PRR::generateRule(ruletext,error);
                    if (rule)
                    {
                        PRR::RuleSet::RULE *prRule = new PRR::RuleSet::RULE();
                        prRule->ruleText = wCategory;
                        prRule->rule = rule;
                        
                        ruleSets[wCategory]->addrule(prRule);
                        std::cout << std::endl << "...## " << ruletext.c_str();
                    }
                    else
                        std::cout << std::endl << "..." << error.c_str();
                }
                catch (...)
                {
                    std::cout << std::endl << "..Exception while generating rule set for rule: " << ruletext;
                    break;
                }
            }
        }
        catch(...)
		{
        	std::wcout << std::endl << L"Exception while generating rule sets";
		}
	}

    //private function: Evaluate a code unit
    void evaluateCodeUnit(CCodeUnit* codeUnit, std::map<std::wstring, PRR::RuleSet*>& ruleSets, PRR::RuleContext* rulecontext, std::wofstream& resultsFile)
    {
        bool codeUnitInitialized = false;
        if (codeUnit != NULL)
        {
            std::wcout << std::endl << L".Code unit - Type: " << codeUnit->getType() << L", Info: " << codeUnit->getInfo();
            if (resultsFile.good())
                resultsFile << L"***  Code unit - Type: " << codeUnit->getType() << L", Info: " << codeUnit->getInfo() << L"  ***" << L"\r\n";
            
            if(ruleSets.find(codeUnit->getType()) != ruleSets.end())
            {
                std::wcout << std::endl << L"..Evaluating rules...";
                PRR::RuleInterface *codeUnitObject = NULL;
                
                if(codeUnit->getType() == L"Class" || codeUnit->getType() == L"Struct")
                    codeUnitObject = new CRVClass(codeUnit, codeUnitInitialized);
                else if (codeUnit->getType() == L"Function")
                    codeUnitObject = new CRVFunction(codeUnit, codeUnitInitialized);
                
                //execute rules on the code unit only if the code unit is initialized.
                if (codeUnitObject)
                {
                    if (!codeUnitInitialized)
                    {
                        delete codeUnitObject;
                        
                        std::wcout << std::endl << L"..Empty or invalid code unit...";
                        if (resultsFile.good())
                        {
                            resultsFile << L"No rules evaluated. Incomplete code unit ! " << L"\r\n";
                            resultsFile << L"\r\n";
                        }
                    }
                    else
                    {
                        PRR::RuleVariable* rulevariable = new PRR::RuleVariable();
                        rulevariable->add(codeUnit->getType(),codeUnitObject);
                        rulecontext->setruleset(ruleSets[codeUnit->getType()]);
                        rulecontext->setrulevariable(rulevariable);
                        
                        std::wstringstream ost;
                        rulecontext->evaluate(ost);
                        
                        //write to results file
                        std::wcout << std::endl << L"..Writing to results file..";
                        if (resultsFile.good())
                            resultsFile << ost.str() << L"\r\n";
                        
                        if (rulevariable)
                            delete rulevariable;
                    }
                }
            }
            else
            {
                std::wcout << std::endl << L"..No rules to evaluate ! " << std::endl;
                if (resultsFile.good())
                {
                    resultsFile << L"..No rules to evaluate ! " << L"\r\n";
                    resultsFile << L"\r\n";
                }
            }
        }
    }

    
//Evaluate rule set
void evaluateRuleSet(CFile* codeFile, std::map<std::wstring, PRR::RuleSet*>& ruleSets, std::string outFileName)
    {
        try {
            std::wcout << std::endl << std::endl << L"Evaluating rule sets";
            
            //open output file if valid filename
            std::wofstream resultsFile(outFileName,std::fstream::in | std::fstream::out | std::fstream::trunc);
            //if (!outFileName.empty())
            //    resultsFile = new std::wofstream(outFileName);
            
            PRR::RuleContext* rulecontext = PRR::RuleContext::getInstance();

            for(std::list<CCodeUnit*>::iterator it = codeFile->codeUnits.begin(); it != codeFile->codeUnits.end(); ++it)
            {
                CCodeUnit* codeUnit = *it;
                evaluateCodeUnit(codeUnit, ruleSets, rulecontext, resultsFile);
                
                //evaluate child code units
                if(codeUnit)
                {
                    std::list<CCodeUnit*> childCodeUnits;
                    codeUnit->getChildCodeUnits(childCodeUnits);
                    for(std::list<CCodeUnit*>::iterator it1 = childCodeUnits.begin(); it1 != childCodeUnits.end(); ++it1)
                    {
                        CCodeUnit* codeUnit1 = *it1;
                        evaluateCodeUnit(codeUnit1, ruleSets, rulecontext, resultsFile);
                    }
                }
                
                /*
                //handle namespaces which contains child code units - we may need to rewrite this code to make it generic
                if (codeUnit && (dynamic_cast<CNamespace*>(codeUnit) != NULL))
                {
                    CNamespace* crnamespace = dynamic_cast<CNamespace*>(codeUnit);
                    for(std::list<CCodeUnit*>::iterator it1 = crnamespace->codeUnits.begin(); it1 != crnamespace->codeUnits.end(); ++it1)
                    {
                        CCodeUnit* codeUnit1 = *it1;
                        evaluateCodeUnit(codeUnit1, ruleSets, rulecontext, resultsFile);
                    }
                }
                 */
            }
            
        } catch (...)
        {
            std::wcout << std::endl << L"Exception while evaluating rule sets !";
        }
        
        std::wcout << std::endl << L"..evaluateRuleSet exit..";
    }

    
    
//Parse source code
CFile* parseSourceCode(std::string fileName)
    {
    CFile *codeFile = NULL;
    std::wstring errorString;
    std::wcout << std::endl << L"Parsing source code from file...";
    try
	{
        codeFile = parseCode(fileName, errorString);
        if (codeFile)
            std::wcout << std::endl << L"Number of code units : " << codeFile->codeUnits.size();
        //std::wcout << std::endl << errorString;
	} 
	catch(...)
	{
        codeFile = NULL;
        std::wcout << std::endl << L"Exception while parsing source code !";
	}

    return codeFile;
    }

}

